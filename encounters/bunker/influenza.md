# Influenza

Johnny the Mouse, ein sehr talentierter Influencer, hatte zuletzt einfach Pech
mit den richtigen Werbeverträgen. Seine Followerzahlen sinken konstant. Der
Agent, bei dem er unter Vertrag ist, will nicht, dass seine Investition umsonst
war. Also muss neue Aufmerksamkeit her.

Der Agent heuert Shadowrunner:innen an, die in den Schatten durchaus bereits
einen Namen haben. Sie sollen Johnny und andere Geiseln in einem Museum
kidnappen. Die Entführung soll so lange wie möglich dauern, um ein Spektakel
draus zu machen. Dadurch, dass Johnny von innen filmen kann, sind seine
Nachrichten exklusiv.

Als Extra-Bezahlung springen noch ein paar teure Kunstwerke bei raus, die in
dem Museum lagern, und das Lösegeld für die Geiseln, die im Beifang landen.

## Ablauf

### Ankunft im Museum

Alle Runner haben vermutlich unterschiedliche Gründe, aus denen sie sich mit
Actionfilmen des Nationalsozialismus auseinandersetzen wollen. Falls jmd nichts
einfällt:

- Als Reaktion auf personalisierte Werbung: "Geil, Action-Filme und Nazis!"
- Schnapsidee auf Drogen / du wachst verkatert in einem Bunker auf und stellst
  fest, es ist ein Museum
- Besinnung, Reflektion des Faschismus in einem selbst
- Von Eltern verdonnert
- Man begleitet kleine Geschwister die das wissen wollen
- Treffen für eine Drogen-/Geldübergabe, Treffen mit Schmidt?
- Geschichtliches Interesse / Komm, wir bilden uns jetzt mal!
- Ein Date (kann ja auch was mit Freunden sein)

Das Museum ist in einem Bunker, es geht um die Rolle von Action-Filmen zwischen
1933-45.
- Eingang ist eine große Runde Scheibe, die offen steht; echte Zugangskontrolle
  ist eine Glastür
- Sie geht automatisch auf, wenn virtuelle personalisierte Eintrittskarte/Marke
  auf dem Host hat
- Dahinter geht es in einen langen Gang, und von dort in die Ausstellung.
- Alte Nazi-Devotionalien sind ausgestellt; dazu gibt es AROs, die einem
  zusätzliche Infos anzeigen oder vorsprechen.
  - Thule-Dolch (ist aber nichtmagisch)
  - SS-Uniformen
  - Ausgabe von Mein Kampf
  - Judenstern
  - Stolpersteine
- Es gibt einen Vorführraum für Leni-Riefenstahl-Filme; werden aber nur 
  kommentiert gezeigt.

### Und Action! die Geiselnahme

Die Geiselnehmer sind 6 Runner und tragen bunte Strumpfmasken:

* 1 Adeptin: Feather, weiß
  * encounters/ubahn/leader.png
  * Feather: pragmatische Shadowrunnerin, gerade frisch aus Big Willy, hatte
    früher einen krassen Ruf und will sich wieder beweisen
* 1 Straßenschamane, lila 
  * characters/radu-lumanesc.png
  * Nomade: frustrierter Öko-Schamane. mittlerweile ohne Respekt für
    Zivilisation, Städte, und Menschheit.
* 1 Deckerin, gelb
  * encounters/bunker/gelb.png, flugzeuge 3. Läuferdrohne mit schwerem MG.
  * Krass vercyberte Kampfdeckerin & Riggerin; lebt nur noch fürs Adrenalin.
* 3 Kromeboyz, blau, rot, grün
  * encounters/ubahn/expert-ganger.png
  * Brüder, früher Spezialeinheit, rausgeworfen weil Nazis.

Die Deckerin hat einen Tiefenhack gegen den Museumshost gemacht, damit das
Sicherheitssystem für die Entführer arbeitet.

lila, gelb, blau, Läuferdrohne, Erdgeist 6 (GRW 302) warten am Ausgang, damit niemand entkommen kann.

weiß, rot, und grün gehen durch die Gänge, um alle Geiseln festzusetzen und
zusammenzutreiben.

gelb hilft ihnen dabei durch die Kameras.

Geiseln:
- Mensch, späte 20er, Markenklamotten: Johnny the Mouse, Influencer
- Mensch, anfang 40, Cyberaugen, Anzug + Jeans: Jacob Leupold, CEO von
  Flappflight, der früher bei Saeder-Krupp war, und jetzt sein eigenes Startup
  aufgemacht hat.
- Ork, anfang 20, Rock + Rollkragenpulli: Annika Kerem, Geschichts-Lehrerin,
  scoutet für Exkursion mit ihrer Schulklasse.
- Zwerg, Teenager, weiter Antifa-Pulli, Undercut. Em Honnek, Schüli.
- Elf, Ende 50, graues Kostüm, weiße Bluse, formell. Gertrude Fromm,
  Museumsangestellte, macht die Führungen.

### Die Reaktion

Was sind die Angriffswellen? Es sind immer ein paar Stunden dazwischen.
- Kampf: Die Sicherheitsfirma taucht auf, um Jacob Leupold zu beschützen, und
  ballert stümperhaft durch die Gegend.
  - Danach verschließt lila den Haupteingang mit einer physischen Barriere KS 8.
- Matrix: Decker versucht Kontrolle über Sicherheitssystem zu erlangen
- Social: Polizeipsychologe
- Magie: Ein Polizeimagier versucht, das ganze gewaltfrei zu lösen, und gehen
  im Astralraum rein, um Gedankenkontrolle auszuprobieren
- Stealth: Nachts versucht ein kleines Einsatzteam einzubrechen
- Wenn all das scheitert, lässt sich die Polizei auf Lösegeld ein

Die Entführer behaupten jeden Tag, sie würden es aus unterschiedlichen Gründen
machen. Sie fordern nach einander unterschiedliche Sachen:
1. Rot-Schwarze Front: lasst unsere Genoss:innen in Big Willy frei - Liste mit 
   12 Namen. Die Gruppe kennt man nicht, die Gefangenen sind nicht alle politisch
2. sie befragen Leupold nach den Passwörtern, mit denen sie an seine Blueprints
   kommen; Johnny schneidet das heimlich mit, und macht nen VLog, ob sie nicht
   doch Wirtschaftsspionage betreiben? Sind sie vllt im Auftrag von Saeder-Krupp
   hier?
3. Als einer der Kromeboyz verletzt wird, filmt Johnny sein SS-Tattoo ab - ist
   es eine false flag der Nationalen Aktion, um auf linken Terror aufmerksam zu
   machen?

All das feuert natürlich die Spekulation an; Johnny kann noch Monate später die
verschiedenen Verschwörungstheorien analysieren.

### Anderes Zwischenspiel


Können einzelne Geiseln die Geiselnehmer überzeugen, dass sie ja mithelfen können
und auf ihrer Seite stehen?

Nach der Secu-Welle bemerken die Runner, dass Leupold ja CEO ist, und lila
bezaubert ihn, die Passwörter rauszurücken. Leupold ist aufgelöst, Johnny macht
einen spekulativen VLog drüber.

gelb nennt Feather (weiß) aus Versehen mit Runner-Namen.

lila kommt rein und fängt mit Magiekundigen ein Gespräch über magische Ausbildung
und Zivilisation an.

Johnny macht die ganze Zeit VLogs über die Situation.
- Johnny the Mouse startet ein Crowdfunding, um sein Lösegeld aufzutreiben, und
  bittet andere Influencer um Solidarität
- Laut seiner Aura scheint Johnny keine Angst zu haben, sondern sich über die 
  Situation zu freuen.
- Warum wird das eig zugelassen? bei kritischen Fragen sagen die Kromeboyz nur 
  "Schnauze halten!"
- Johnny kriegt sein Kommlink wenn er es braucht
- Johnny bittet, aufs Klo zu müssen und wird vorgelassen
- Chatverlauf auf den Kommlinks von Johnny und den Geiselnehmern

Sobald die Polizei einen Lösegelddeal anbietet, nehmen die Geiselnehmer ihn an
und handeln aus, mit einem Heli abzuhauen.

#### NPCs

##### Sicherheitsfirma

encounters/bunker/security.png

##### Polizei

HanSec braucht ca. 20 Minuten um aufzutauchen. Gehen hinter ihren Wannen in Deckung und zielen auf den Eingang. Etwas dahinter steht Wiesengrund mit Megafon.

Decker: encounters/bunker/polizeidecker.png

Polizei-Magier & -Psychologe: Isaak Wiesengrund.
- Er trägt ein Tweed-Jackett mit Ellenbogenpolstern. Keine Pfeife. Er sieht ziemlich durchschnittlich aus.
- Teddy ist ein neugieriger Mensch, der sich bis ins Detail in ein Thema hineinfuchsen kann.
- Er war es lange nicht gewohnt, für seine Prinzipien auch einstehen zu müssen.
- doch nun erkennt er seine Verantwortung, und seit er nichts mehr zu verlieren hat, fällt ihm einfacher, Opfer zu bringen.
- Er ist dankbar und loyal, wenn ihm jemand geholfen hat, zeigt er sich erkenntlich.
- Er ist charismatisch und in der Lage, seine psychologische Theorie und Praxis für Manipulation einzusetzen.
- encounters/bunker/wiesengrund.png

Spezialeinheit: encounters/bunker/spezialeinheit.png

## Matrix

### Museum Host

Hoststufe 4 - asdf 4576

* Zutritt zum Host kostet eine personalisierte Eintrittskarte, die auch für das
  physische Museum gilt.
* Host ist vergleichsweise altbacken gestaltet, Mindestens 5 Jahre alter Look.
* Akkurate Repräsentation des echten Museums
* Gemälde an der Wand: Zusatzinfo zu den Kunstwerken
* Der Host wird anscheinend nicht selbst künstlerisch verwendet, sondern dient
  eher als Info. Kein Multimedia-Konzept.
* Slaves: Kameras, Türen, Alarm
* IC: Patrouille, Marker, Aufspüren, Wirbel

Eingang zum Fundament ist einem Kunstwerk versteckt.

#### Fundament

- Man steht in einem zweidimensionalen Porträt, alles sieht sehr altmodisch aus.
- Es ist ein Raum, mit einem Fenster, durch das trübes Licht scheint
- Es liegt ziemlich viel Kram rum, Kisten, Klamotten, Hüte, Kissen, Pinsel, Felle.
- Sieben Malerinnen sitzen vor sieben Leinwänden, alle zeigen den Raum.
- Sie tragen alle extravagante Hüte, mit Federn, und in unterschiedlichen Farben.
- Ihre Pinsel sind alle ca. einen halben Meter lang, und sie haben keine Gesichter.
- Aber sie malen alle unterschiedliche Malerinnen, keiner malt alle sechs anderen.

Es ist ein rekursives Gemälde, wenn man Malerinnen in einem der Gemälde beeinflusst,
kann man auch die korrespondieren Maler im Gesamtgemälde beeinflussen.

Abweichungen:
- Einen Hut ohne Feder tragen.
- Eine der Malerinnen ansprechen.
- Über etwas anderes als Kunst reden.
- Mit Farbe malen.
- In den Gemälden herummalen (schwer)

##### Bewohner

Die Malerinnen haben ihre Attribute auf 4, und werden schnell zickig, wenn sie
sich gestört fühlen. Sie können sich allerdings nur durch Gestik ausdrücken.

##### Knoten

Die Knoten sind die Hüte der Malerinnen, die Datenpfade laufen über den Arm &
Pinsel der Malerin in das Gemälde, wo sie die 1-3 Hüte malen, zu denen die
Daten fließen sollen.

- Archiv: Orange
- Sicherheitszentrale: Rot
- Slave-Steuerung: Violett
- Hauptkontrolle: Grün
- Null-Knoten: Schwarz
- Bühnensteuerung: Gelb
- Portal: Blau

Um die Knoten zu beeinflussen, muss man sie anmalen - man darf aber keine Farbe
benutzen, sondern den Pinsel nur in Wasser tauchen.

