# Kreuzfahrt

Hab ja keine Ahnung von Kreuzfahrten - weist mich gerne darauf hin, wenn was unrealistisch ist :D

Triggerwarnung: Erwähnung von Kindesmissbrauch, es geht in dem Abenteuer tatsächlich relativ viel darum.

## Hintergrund

Die Spieler/innen sind auf einem Kreuzfahrtschiff, das in Hamburg gestartet
ist, und Richtung Kanarische Inseln unterwegs ist. Sie kennen sich nicht
unbedingt.

Die Reise verläuft normal, bis sich plötzlich ein Ehepaar meldet,
dass ihre Tochter nicht aufzufinden ist. Ihr Rollstuhl und ihr Kommlink werden
unter Deck gefunden - es sieht nicht nach Selbstmord oder Unfall aus, sondern
nach einer Entführung.

Die Entführer/innen haben das ganze gut geplant:
- Sven lässt seine Zaubershow mit Trugbild enden, für dass er nicht da sein
  muss. Er geht kurz rauf auf die Brücke zu Cem.
- Gloria klopft bei Hannah, hebt sie kurzerhand aus dem Rollstuhl, und
  versteckt sie 4 Türen weiter in ihrer Kabine, geknebelt & gefesselt.
- Sven bittet Cem, mal einen Blick auf sein Kommlink zu werfen, der schraubt es
  auf. Sven steckt solange einen Stick in Cems Deck. Der lädt einen Agenten,
  der immer wieder alle Überwachungs-Aufzeichnungen löscht.
- Sven geht rechtzeitig runter und beendet die Show.
- Kurz darauf wird der leere Rollstuhl im Zimmer mit offener Tür gefunden.

Heikel ist, dass die Mutter der Entführten Sprecherin eines Verbands für die
Rechte von pädophilen Nicht-Tätern ist. Die beiden Entführer/innen kennen sich
aus einer Selbsthilfegruppe für Missbrauchsopfer. Sie vermuten, dass die
Tochter missbraucht wird, und wollen sie retten.

## Events

### Der Karaoke-Nachmittag

Sven Holm moderiert. Er sucht Sänger/in für die Schiffs-Band. Es gibt nur
Trash-Lieder. Es sind alle interessanten Leute bei der Vorstellung, bis auf
Kapitän Ritter.

https://shadowrunberlin.wordpress.com/2012/05/29/36-zufallssongs/

Während des Karaoke-Abends fordert Sven Hannah for allen Leuten auf, doch auf
die (nicht barrierefreie) Bühne zu kommen. Er ist sehr subtil creepy, es ist
ihr sichtlich unangenehm. Sie fährt unter Deck; ihre Eltern bleiben da.

Ansonsten wird am Ende der Karaoke, wenn ein/e Spieler/in gesungen hat, sie/er
von Sven auf der Bühne darauf angesprochen, ob sie/er nicht bei der
Schiffs-Band einspringen will.

### Die Entführung

Am Abend dann: Unterhaltung. Sven zeigt Zaubershow. Endet mit längerem
Trugbild, danach tritt Sven wieder auf die Bühne und ruft zum Bar-Abend auf.

15 Minuten später: Rollstuhl wird leer unter Deck gefunden.
Hannas Kommlink liegt daneben.
Überwachungskamera-Videos werden die ganze Zeit von einem Agenten in Form eines
Radiergummis mit Armen & Beinen gelöscht.

Alle Passagiere sollen auf ihre Kabinen; Securities suchen das Schiff ab. Sonst
passiert nichts mehr.

Nachts bricht Sven in die Küche ein und klaut Essen; er hinterlegt es auf dem
Frauenklo in -2. Kurz darauf geht Gloria in -2 auf die Toilette, und nimmt das
Essen mit in ihre Kabine.

### Durchsuchungen der Kabinen

Am nächsten Morgen will Kapitän Ritter alle Kabinen durchsuchen lassen, aber
plötzlich hält das Schiff, und Ruben Waterkant verkündet, dass er streikt, wenn
es Durchsuchungen gibt, weil Durchsuchungen scheiße sind, lässt nicht mit sich
reden, und droht sogar Sabotage an.

Sven Holm überzeugt Ritter, dass Ruben sich nicht umstimmen lassen wird, und man
das Ganze aus juristischen Gründen lieber im nächsten Hafen machen soll. Ritter
befiehlt Kurswechsel Richtung Porto. Ankunft in voraussichtlich 20 Stunden.

Wenn Leute vorschlagen, ihre Kabinen freiwillig durchsuchen zu lassen, ruft
jemand dazwischen: "Und wenn sie fliehen wollte? Immerhin ist Frau Heinrich
pädophil!"

Spätestens wenn der Kapitän rausfindet, dass Frau Heinrich Sprecherin von RePNT
ist, hört er auf, ihr zu helfen. Danach setzt sie eine Belohnung in Höhe von
10000 Nuyen für das Auffinden ihrer Tochter aus.

Falls die Runner auf die Idee kommen, die Kabinen mittels astraler Projektion
zu durchsuchen - es sind ziemlich viele Leute seekrank, falls sie auf der Basis
"da ist jemand krank" die Kabine aufbrechen, finden sie eine seekranke Person.

### Femme Fatale

Vormittags bricht am Pool ein Streit aus. Gloria Thum ist von Mike und seinen
vier Bros umringt, die lautstark auf sie einreden. Da schubst Mike sie mit
voller Wucht in den Pool. Die Leute lachen sie aus, "geschieht dir Recht,
Chromschlampe!"

Bei dem Streit ging es darum, dass Gloria zu denen gehörte, die ihre Kabine
nicht freiwillig durchsuchen lassen wollten. Die anderen fanden das
verdächtig. Wenn man sie darauf anspricht, sagt sie i-wann, dass sie selbst eine
Vergangenheit mit Missbrauch hat, und nicht darüber reden will.

Ansonsten freut sie sich über Solidarisierung, und ergreift auch die
Gelegenheit, die Typen noch zu verprügeln, wenn sie nicht alleine dasteht.

### Nachmittags

Band-Probe mit Sven?
Gloria muss Windeln wechseln?

Matthias Heinrich wird auf Deck -1 oder -2
von zwei vermummten Bros aus Mikes Crew festgehalten.
Ein dritter nimmt ihm das Kommlink (Hermes Ikon, Gerätestufe 5) aus der Tasche,
entsperrt es mit Matthias' Fingerabdruck,
die drei hauen ab.
Die Überwachungskameras haben sie vorher mit Rasierschaum zugesprüht.

Matthias Heinrich bittet die Held:innen, es wiederzufinden.
Mike hat das Kommlink noch an,
sitzt mit seinen Bros in seiner Kabine auf Deck -2
und findet darauf AI-generierte Kinderpornos,
und ein paar heimlich aufgenommene Fotos auf denen man niemand erkennt.

### Der Band-Auftritt

Abends tritt die Schiffs-Band auf. Wen haben sie gefunden? Spieler/in?

Am Ende sagt Sven, dass das Schiff morgen früh in Porto einläuft, und dass sich
alle um 11:00 auf Deck 0 einfinden sollen, für ein besonderes Schauspiel.

Danach erleichterter Bar-Abend mit Sven, Ruben, und Cem. Gloria trinkt alleine an
der Bar. Wenn sie alleine dort sitzen bleibt, fängt sie i-wann betrunken
Streit mit dem Typen an, der sie in den Pool geschubst hat.

### Landung

Vormittags anlegen in Porto; wie schmuggeln Sven & Gloria Hannah von Deck?

gegen 11 Uhr wird am Horizont Porto sichtbar; Sven ruft Passagiere an Deck, um die Anfahrt zu beobachten (Gloria fehlt). Er erklärt:
- In einer halben Stunde laufen wir in den Hafen ein.
- Schön, dass wir alle hier versammelt sind, um den Anblick zu erleben.
- Um das noch aufzupeppen, habe ich eine magische Performance vorbereitet - schaut aufs Wasser!

Sven stellt sich an die Reling, setzt seinen Zylinder auf, hebt die Arme,
schließt die Augen, und beginnt zu murmeln.
- Erst brodelt es nur an einer Stelle im Wasser, dann schäumt es.
- Plötzlich ein Platscher, und Wasser spritzt hoch in die Luft.
- doch in der Luft, fällt es nicht wieder herunter - es beginnt sich um sich selbst zu drehen
- Das Publikum starrt mit offenem Mund
- schnell entsteht eine Wassersäule, ein Tornado, der sich immer schneller dreht, und beginnt auf das Schiff zuzueilen
- Der Tornado trifft das Schiff, das Wasser bricht, doch bevor es aufs Deck & Publikum platscht, werden es Schmetterlinge.

Sven dreht sich um, wedelt mit den Armen, verbeugt sich, & lüpft den Hut. Die Menschen klatschen.
- Als er wieder hochkommt, ändert sich sein Gesichtsausdruck.
- Sven zeigt hinter das Publikum, geschockt - "äääh... das da ist allerdings keine Illusion!"
- Über die Reling klettern drei schwarz vermummte Kämpfer - arabische Schrift auf der Kleidung, Sturmgewehre auf dem Rücken.
- Einer schießt eine Salve in die Luft: "Allahu Akbar!"
- Aus den beiden Treppenhäusern kommen auch vermummte Kämpfer.
- Die Leute fangen an zu kreischen.

Die Terroristen sind auch nur ein Trugbild von Sven, um von Gloria abzulenken
und Chaos auszulösen. Am Ende wird er sagen, es war doch nur ein Scherz, und
gehörte zur Show. Es bestand nie Gefahr.

Gloria eilt Richtung Maschinenraum. Sie ruft Ruben nach oben, es gäbe einen
Angriff, er wird gebraucht.  Als er weg ist, schnappt sie sich die geknebelte
Hannah, nimmt sie mit in eine Rettungskapsel, und stößt sich vom Schiff ab.

Während die Aufregung noch andauert, kommt Orzak mit einem Schnellboot
angedüst, nimmt die Rettungskapsel in Schlepptau, und fährt wieder weg. Gloria
versteckt sich mit Hannah bei in Orzaks Safehourse in Porto.

### Finden

Die Kabine sieht aus wie viele hier
- 4 Betten, in Form von 2 Hochbetten
- kleine Luke als Fenster
- auf einem der unteren Betten liegt Hannah, geknebelt, die Arme gefesselt.
- sie schaut euch mit großen Augen an und macht undeutliche Geräusche.
- Wahrnehmung (3): leichter Geruch nach menschlichen Ausscheidungen

Wenn man sie losbindet:
- "Dem Herr sei Dank, ihr kommt um mich hier rauszuholen?"
- Sie will zurück zu ihrer Mutter; von Vater sagt sie nichts
- Die Entführerin hat gesagt, meine Eltern tun mir Gewalt an, aber das sind
  doch meine Eltern?
- Sie müsste mal wieder gewickelt werden; ihre Eltern haben ihr nie
  beigebracht, wie sie sich selbst säubert

Wenn man sie nicht losbindet, schaut sie enttäuscht, macht weiter ihre
Geräusche, und wird i-wann geistesabwesend.

### Retten

Wenn man Hannah zu ihren Eltern bringt:
- Mutter reagiert sofort und jauchzt vor Freude, ihre Tochter wiederzukriegen.
- Herr Heinrich stürmt auf sie zu und nimmt sie in den Arm. Wahrnehmung
  (2): Hannah schaut dabei skeptisch.

Frau Heinrich geht zu den Spieler/innen:
- danke vielmals, ich weiß nicht, was wir ohne euch gemacht hätten
- waren krank vor Sorge
- gut, dass sie unsere kleine wieder zu ihrer Familie gebracht haben
- was bleibt einem denn außer die Familie
- wie kann ich Ihnen die Belohnung zukommen lassen?

## Dramatis Personae

Claudia Heinrich (35), Mensch
- Blonde Locken, Schmuck.
- Wenn Spieler/innen Auge für sowas haben: Kleidung soll teuer aussehen (ist es aber nicht)
- 2: Sprecherin des Verbands für die Rechte von Pädophilen Nicht-Tätern (RePNT)
- 3: Arbeitet in der Buchhaltung
- 4: Schönheits-OPs um jünger zu wirken
- Liebt Vater, relativ unerwidert
- Sie hat i-wann herausgefunden, dass er auf Kinder steht, & setzt sich seitdem
  für seine Rechte ein

Matthias Heinrich (47), Mensch
- Halbglatze, zurückhaltend gekleidet
- 3: Physikdozent in Jena
- Ungeoutet Pädophil
- Ist Zweckehe mit Mutter eingegangen

Hannah Heinrich (10), Mensch
- Sitzt im Rollstuhl
- Unfall - "wir können uns die OP nicht leisten. Ares hat uns angeboten die Cyberware zu sponsern, gegen einen 20-jährigen unkündbaren Arbeitsvertrag, und sie wäre immer von Immunsupressiva abhängig - bisher konnten wir uns noch nicht dazu durchringen."
- Liebt Mutter

Sven Holm (32), Zwerg
- trägt schwarzen Zylinder und Jacket, wirkt dadurch noch kleiner
- Als Animateur auf dem Schiff angestellt - gutes Face - verantwortlich für das ganze Unterhaltungsprogramm
- 3: Aspektmagier, kommt aus einem Förderprogramm für erwachte Sprawl-Kids
- zeigt Zaubershows für Reisende
- 5: Missbrauchserfahrung im Kindesalter
- 6: Hat Gloria bei Selbsthilfegruppe kennengelernt
- hilft Gloria bei ihrem Plan

Gloria Thum (30), Ork
- Trägt immer einen Jeansmantel, eig unpassend für Kreuzfahrt. daraus ragen Cyber-Hände hervor. Schaut unzufrieden
- Stark vercybert - Distanz zum eigenen Körper
- 3: Ex-Runnerin, saß in Big Willy, ist i-wann ausgestiegen (Connections: Shadowrunner HH?)
- 6: Missbrauchserfahrung im Kindesalter
- Versucht jetzt Kinder vor Missbrauch zu schützen (Connections: Shadowrunner HH)
- Ist auf Kreuzfahrt gegangen, um Hannah zu "retten"

Cem Kleist (34), Elf
- klein für einen Elf, Brille, trägt einen scheinbar selbstgestrickten Pullover
- Sicherheitspinne
- Gutgläubig, sucht Anschluss, nicht viel Lebenserfahrung
- 2: Teilt Posts der Liberaldemokratischen Föderalistischen Partei (LDFP)
- 4: Hat in Karlsruhe Matrixsicherheit studiert

Endo Akuri (44), Mensch
- hager, Es-Pronomen-Button, 3mm-Frisur, trägt selbst innen eine Sonnenbrille.
- Rigger, steuert das Schiff
- schweigsam
- 1: Veröffentlicht ab und zu Beats auf Soundkraut
- 2: Teilt Posts über LGBT-Rechte und Metarassismus
- 4: Hat in Karlsruhe Maschinenbau studiert
- Ziemlich viele Sicherheitsdrohnen

Sigmar Ritter (55)
- Kapitän, Schmucke blaue Uniform+Mütze, weißer Bart, flucht viel

Security
- Blaue Uniformen, Taser, strahlen Aura aus, dass man sie nicht ansprechen will

Mike (28)
- ist mit seinen 4 Kommilitonen auf Kreuzfahrt, wollen Party machen / sich daneben benehmen
- etwas enttäuscht und gelangweilt, weil so wenig Frauen in seinem Alter da sind
- completely full of himself, glaubt dass niemand außer ihm den Fall ebenfalls lösen kann
- 1: BWL-Student auf einer Ares-Uni
- 2: Scheint auf True-Crime-Trideos zu stehen
- 3: Seine Eltern arbeiten auch bei Ares im mittleren Management

Ruben Waterkant (18), Troll
- Baggys, weiter Hoodie, Hände in den Taschen. 
- hervorstehende Hauer lassen ihn aggressiv aussehen. 
- Tätowierte Träne im Gesicht.
- Ist für den Antrieb des Schiffs zuständig: Wassergeister als Turbinen
- Aspektmagier
- 2: Chaosmagier, das Wasser war ihm schon immer am nächsten; i-wie hat er sich immer durchgeschlagen.
- 3: kommt aus einem Förderprogramm für erwachte Sprawl-Kids
- Unzuverlässig, launisch, genervt, weil der Astralraum im umgekippten Meer sich so scheiße anfühlt
- Streikt, als der Kapitän Durchsuchung der Kabinen anordnet; hat zu viele Hausdurchsuchungen erlebt

Schiffs-Band
- Eine schweigsame Security-Orkin spielt Bass (Irenka Popolski)
- Endo Akuri spielt mit mehreren Drum-Machines auf einmal
- Sven Holm spielt Keyboard
- Sänger wurde krank und ist nicht mit auf der Reise, deswegen suchen sie jmd für den Gesang

Oorzak (34) Elfe
- kojote S.23
- 5: Anarchistin aus der Russischen "Republik", die abhauen musste (Connections: Anarchisten, Shadowrunner)
- Arbeitet seitdem als Kojote in Hafenstädten
- Redet wenig, außer es geht um Politik
- Hat ein leises Aztech Nightrunner Schnellboot und ein Safehouse in Porto

## Das Schiff - die Amphitere III

Die Amphitere III gehört der etwas heruntergekommenen Kreuzfahrtlinie Amphitere
Inc. Sie wird durch Wassergeister angetrieben, aber über die Matrix gesteuert.
Sie hat 5 Decks + ein
"Maschinenraum-Deck", in dem Ruben Waterkant die Geister beschwört, und in dem
sie die Turbinen antreiben. Es gibt zwei Aufzüge zum rauf- und runterfahren in
der Mitte, sowie Treppen vorne und hinten.

Amphitere III: Rumpf 50, Panzerung 14

### Deck 2: Brücke + Schlafräume der Crew

Steuerraum, wo man sich per Direktverbindung einstecken kann.

Schlafräume der Crew; es gibt 20 Kabinen, wo jeweils 1 Person hineinpasst.

### Deck 1: Bühne + Bar

Sven Holm ist verantwortlich für das Programm der Bühne, es ist auch Raum
dafür, selbst etwas einzubringen.

Bühne
- Zaubershow
- Karaoke
- Die Schiffs-Band
- Filmabende
- Poetry Slam-Abende

### Deck 0: Pool + Restaurant

Restaurant ist drinnen, Pool auf dem Deck. Drumrum stehen Tische, Stühle und
Liegestühle fürs Sonnen, die allerdings keine Orks & Trolle aushalten und dann
schon mal zusammenkrachen.

Hinten im Restaurant ist die Küche. Es gibt 2 Sorten Gerichte zu jeder
Mahlzeit; ein teures aus organischen Zutaten, und eins aus Soykram. Die
Securities wechseln sich mit Kochen ab.

### Deck -1: Kabinen

Hier gibt es 30 Familien-Kabinen mit Stockbetten, in die pro Kabine vier
Metamenschen passen. 

20 weitere Kabinen sind erste Klasse; in sie passen nur zwei Metamenschen, mit
Ehebetten.

### Deck -2: Kabinen

Hier gibt es weitere 40 Familien-Kabinen. In einer davon wohnen die Heinrichs,
in einer Gloria.

### Deck -3: Maschinenraum

Aufzug fährt nicht bis hier runter, nur die hintere Bord-Treppe reicht bis hier.

- Schmucklos und eng. 
- An den Wänden stehen noch alte Maschinen herum, Jahrzehnte ungenutzt, verstaubt

An der hinteren Schiffswand gibt es drei Einbuchtungen, geschützt durch
Sicherheitsglas (Struktur 8, Panzerung 12). In diesen Einbuchtungen schwimmen
Wassergeister im Meerwasser und treiben durch ihre Bewegungen die Turbinen
hinter ihnen an.
- Sehen aus wie Farbsprudel/Spritzer, die sich konstant mit dem Wasser drumherum vermischen und wieder trennen.
- grün, rot, und blau.

Vor den Einbuchtungen ist mit Spraydosen ein Beschwörungskreis auf den Boden gemalt;
- Grafittis an den Wänden
- Aufkleber mit politischen Botschaften kleben an Pfeilern und Wänden
- Aus einer der alten Maschinen ist eine Schrottskulptur gebastelt

Am Bug sind mehrere Luken; sie führen zu engen, kleinen Rettungs-U-Booten.

## Matrix

### Schiffs-Host Stufe 3 - 4356

- kleine Südsee-Inseln mit Palmen, die im Himmel schweben
- man kann auf Wolken zwischen ihnen umherschweben
- Auf der größten Insel in der Mitte ist eine Strandbar, mit einem großen Info-Logo. Da beantwortet ein Bar-Agent Fragen
- andere Inseln sind für die Brücke, Bibliothek, Steuer, den Toillettenstatus, den Pool, oder wo die Leute noch so unterwegs sind.
- auf einer Insel ist ein großes Steuerrad; das ist das Steuerinterface
- auf einer Insel gibt es eine Bibliothek, Türen immer offen. Bibliothekar (Agent) sucht einem Bücher/Trideos raus.

- offen für Passagiere, die kriegen eine Marke auf dem Host.
- Den Info-Agent an der Bar kann man für einen extra-Preis nach einer
  Privat-Insel im Host fragen
- Steuerhierarchie des Schiffs: Kapitän, Rigger, Decker, Staff, alle anderen.
- Zum Steuern braucht man eine Marke auf dem Steuerrad.
- Passagierliste liegt im Archiv, aber Cem, alle Agenten & IC haben eine Marke
  drauf.
- Um ins Fundament zu kommen, muss man von einer Wolke oder Insel nach unten
  springen.

Patrouillen-ICE checkt alle 1W6 Runden auf den Inseln, ob Icons auf
Schleichfahrt sind, oder ob Icons, die nicht auf der Passagierliste stehen, da
sind. Wenn ja, wird der Alarm ausgelöst.

Wenn der Alarm ausgelöst wird, startet der Host Schwarzes ICE, Aufspüren,
Säure, Leuchtspur, und Absturz. Wenn eins davon abstürzt, startet der Host es
neu, bevor er die Liste weiter abarbeitet. 1W6+3 Runden nach auslösen des
Alarms trifft herculee ein, die Sicherheitsspinne.

