# Endo Akuri (44), Mensch
- hager, Es-Pronomen-Button, 3mm-Frisur, trägt selbst innen eine Sonnenbrille.
- Rigger, steuert das Schiff
- schweigsam
- Ziemlich viele Sicherheitsdrohnen

K G R S - W L I C - Essenz  
3 4 6 2 - 3 4 4 2 - 5

Cyberware:
- Riggerkontrolle Stufe 1. Essenz 1

Ausrüstung:
- Riggerkonsole Stufe 3, D4 F4.

Fähigkeiten:
- Waffenloser Kampf 3 - 7
- Schleichen 5 - 9
- Gebräuche 2 - 4
- Wahrnehmung 4 - 8
- Geschütze 6 - 10
- Flugzeuge 4 - 10
- Schiffe 4 - 10
- Computer 6 - 10

Ini:            8+3d6  
Limits:         5 / 5 / 4  
Zustandmonitor: 10 / 10  
Kommlink:       6  
Ausweichen:     10  
Panzerung:      9  
Überz. Schaden: 3

4x EVO Watchdog Rotordrohne: Rumpf 4, Panzerung 4
- 2 mit Cavalier Arms Crocket EBR Scharfschützengewehr: Präz. 6, 12K, DK -3
- 2 mit Aztechnology Striker: Präz. 5, 24K, DK -4/-10 (Fahrzeuge)

4x MCT Fly-Spy

Schiff Amphitere III - Rumpf 50, Panzerung 14

