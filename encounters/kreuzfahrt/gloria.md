# Gloria Thum (30), Ork
- Stark vercybert - Distanz zum eigenen Körper
- Missbrauchserfahrung im Kindesalter
- Ex-Runnerin, ist i-wann ausgestiegen
- Versucht jetzt Kinder vor Missbrauch zu schützen
- Ist auf Kreuzfahrt gegangen, um Hannah zu "retten"
- Kommlink Stufe 5
- Kommlink: sie hat zuletzt mit Sven Holm (2 Stunden nach der Entführung) und mit Orzak telefoniert (nachdem sie wissen, dass sie nach Porto fahren).

K G/+ R/+ S/+ - W L I C - Essenz  
9 4/9 4/6 5/9 - 3 2 4 2 - 2.1

Cyberware:
- 2 Cyberunterarme, Maßanfertigung (Str&Ges+3), Cybergliedverstärkungen (Str&Ges +3). 0.9 Essenz
- ein Cyber-Springarm mit Defiance EX-Taser: Präz. 4, 9G, DK -5, 4 Schuss
- ein Cybersporn: Str+3K Schaden, -2 DK
- Reflexbooster Stufe 2. 3 Essenz

Panzerung:
- Panzerjacke mit Elektrische Isolierung 6

Fähigkeiten:
- Waffenloser Kampf 6 - 15
- Schleichen 5 - 9
- Gebräuche 2 - 4
- Pistolen 3 - 12
- Wahrnehmung 4 - 8
- Einschüchtern 6 - 8
- Computer 2 - 6

Ini:            8+3d6  
Limits:         9-11 / 4 / 3  
Zustandmonitor: 13 / 10  
Kommlink:       2  
Ausweichen:     10  
Panzerung:      12  
Überz. Schaden: 9

