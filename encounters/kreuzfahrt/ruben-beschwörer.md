# Ruben Waterkant (18), Troll
- Baggys, weiter Hoodie, Hände in den Taschen. 
- hervorstehende Hauer lassen ihn aggressiv aussehen. 
- Tätowierte Träne im Gesicht.
- Ist für den Antrieb des Schiffs zuständig: Wassergeister als Turbinen
- Aspektmagier, kommt aus einem Förderprogramm für erwachte Sprawl-Kids
- Chaosmagier, das Wasser war ihm schon immer am nächsten; i-wie hat er sich immer durchgeschlagen.
- Unzuverlässig, launisch, genervt, weil der Astralraum im umgekippten Meer sich so scheiße anfühlt
- Streikt, als der Kapitän Durchsuchung der Kabinen anordnet; hat zu viele Hausdurchsuchungen erlebt

K G R S - W L I C - M  
7 4 2 4 - 4 2 6 4 - 5

Fähigkeiten:
- Herbeirufen 5 - 10
- Waffenloser Kampf 3 - 7
- Askennen 4 - 10
- Schleichen 3 - 7
- Einschüchtern 6 - 10
- Wahrnehmung 4 - 10
- Computer 4 - 10

Ini:            8+1d6  
Astr. Ini:      8+2d6  
Limits:         6 / 5 / 6  
Zustandmonitor: 12 / 10  
Kommlink:       3  
Ausweichen:     8  
Panzerung:      5  
Überz. Schaden: 7

3 Geister des Wassers Kraftstufe 4
- Sehen aus wie Farbsprudel/Spritzer, die sich konstant mit dem Wasser drumherum vermischen und wieder trennen.
- grün, rot, und blau.

K G R S - W L I C - M Ess  
4 5 6 4 - 4 4 4 4 - 4 4

**Kräfte:** Astrale Gestalt, Bewegung, Bewusstsein, Materialisierung, Suche, Verschleierung, Verschlingen, Verwirrung  
**Fertigkeiten:** Askennen, Astralkampf, Exotische Fernkampfwaffe, Waffenloser Kampf, Wahrnehmung

Ini:            10+2d6  
Astr. Ini:      8+3d6

