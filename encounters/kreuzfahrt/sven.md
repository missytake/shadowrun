# Sven Holm (32), Zwerg
- trägt schwarzen Zylinder und Jacket, wirkt dadurch noch kleiner
- Als Animateur auf dem Schiff angestellt - gutes Face - verantwortlich für das ganze Unterhaltungsprogramm
- Aspektmagier, kommt aus einem Förderprogramm für erwachte Sprawl-Kids
- zeigt Zaubershows für Reisende
- Missbrauchserfahrung im Kindesalter
- Hat Gloria bei Selbsthilfegruppe kennengelernt und hilft ihr

K G R S - W L I C - M  
5 2 4 2 - 6 4 4 7 - 4

Zauber:
- Beeinflussen
- Maske
- Trideo-Trugbild
- Unterhaltung
- Belebung

Fähigkeiten:
- Spruchzauberei 6 - 10
- Askennen 4 - 8
- Waffenloser Kampf 3 - 5
- Schleichen 5 - 7
- Gebräuche 6 - 13
- Wahrnehmung 6 - 10
- Fingerfertigkeit 4 - 6
- Computer 4 - 8
- Vorführung 6 - 13

Ini:            8+1d6  
Astr. Ini:      8+2d6  
Limits:         5 / 6 / 8  
Zustandmonitor: 11 / 11  
Kommlink:       5  
Ausweichen:     8  
Panzerung:      5  
Überz. Schaden: 5

