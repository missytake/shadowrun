# U-Bahn in Berlin: Kundenbefragung

Runner sitzen in einer U-Bahn in Berlin. Kennen sich am besten noch gar nicht
gegenseitig.

Sie werden von Gangern überfallen, die eine Zielperson suchen, die eine
Pädophilen-Forum administriert. Die Ganger kennen nur das Forumsprofil und
wissen, dass das Ziel in der U-Bahn sitzt.

Sie versuchen also, alle zu befragen, um herauszufinden, wer es ist, und die
Person zu entführen. 

## Beschreibung

Die U-Bahn rattert durch den Westen Berlins und ist schon ziemlich in die Jahre
gekommen. In der U-Bahn sitzen ca. 30 Leute. 
- Zwei Obdachlose
- eine kleine Zwergen-Familie mit 1,3 schreienden Kindern
- drei Punks haben in einem der besseren Bezirke echte Kuhmilch containert
- ein lesbisches Pärchen mit mehreren vollen Einkaufstüten
- hier und da sitzen ein paar Ganger breitbeinig herum und hören "No more" von
  Faith (Christpop)
- ein Dutzend Wageslaves im Anzug, mit auffälligen Piercings

[In der U-Bahn sitzen:]
- 7 Ganger
  - 1 Magier
  - 1 Adept (Boss)
  - 3 Ork-Ganger (leicht vercybert)
  - 2 Elf-Ganger (schwer vercybert)
- 3 Runner
- ca. 20 Zivilisten
- 1 Zielperson

- Wohin wollt ihr gerade?
- Was habt ihr an?
- Was habt ihr an so einem Tag dabei?
- Ist euer Kommlink an, Schleichfahrt?
- Könnt und benutzt ihr Masking?

## Überfall

- Plötzlich hält die U-Bahn mitten im Tunnel an
- Kommt quietschend zum stehen
- ruft jemand durch den Waggon "Ruhe bewahren!"
- Die Ganger an beiden Enden des Waggons stehen auf
- einer schießt kurz zwei Mal in die Luft.

[Störsender geht an.]

Ein widerlich blonder Ganger, ein scheinbar unbewaffneter Mensch, steht auf und
läuft entspannt in die Mitte des Waggons: "So, wir werden euch jetzt alle ein
paar Fragen stellen. Die, die sie richtig beantworten, können gehen, wenn wir
gefunden haben, wen wir suchen."

Einer von den Orks raunzt: "Wenn jmd Faxen macht, erschießen wir ihn und
irgendjemand anders."

[Die Angreifer suchen eins der Wageslaves, wissen aber nicht, wen genau.]
- glaubt an Gott
- hat mal auf der Straße gelebt
- kann nicht mit Schusswaffen umgehen
- steht auf Goblin-Rock

Scheuchen erst alle Leute an ein Ende des Zuges. Alle sollen sich hinsetzen,
Arme auf die Schenkel legen.

## Kundenbefragung

Nehmen immer jemand ans Ende des Zuges. Wird vorher grob durchsucht. stellen da
schwer hörbar Fragen, Person soll sich danach ans andere Ende des Zuges setzen.

Der Blonde stellt den Leuten Fragen. Er wirkt ungewöhnlich kultiviert für einen
Ganger. Er hat einen Schreibblock, auf dem er nach jeder Frage Dinge abhakt.

Fragen:

- "Was arbeitest du? Arbeitest du was?"
- "Du bist nicht aus Berlin, oder?"
- Hält Waffe ins Gesicht - "Na, was ist das? Erzähl mir was genaueres."
- "Glaubst du an Gott?"
- "Auf was stehst du? Leute, Männer, Frauen, dazwischen, Möbelstücke?"
- "Was hörst du so für Musik?" - "Und Synth-Pop?" - "Ja, aber Goblin Rock ist schon besser als Synth-Pop, oder?"
- "Was war die schlimmste Absteige, in der du je gewohnt hast?" - "Ja, aber besser als auf der Straße, oder?" - "Keine Erfahrung damit?"

[Wenn jemand nicht ins Profil passt, soll die Person noch ein, zwei Fragen
beantworten und sich dann hinsetzen. Wenn jemand bis zum Ende alle
Anforderungen erfüllt, kommt sie auf eine andere Bank. Die Leute auf der Bank
werden ganz am Ende entführt und ihre Kommlinks nochmal im Detail gescant.]

## Magier:

K	G	R	S	W	L	I	C	M	E  
5	2	5	2	7	7	5	2	8	3

Initiative: 10+1d6  
Panzerung: 15 	Zustandsmonitor: 11/10	Hermetiker 	Mensch  
Antimagie 6, Askennen 4, Astralkampf 5, Spruchzauberei 6, Wahrnehmung 3, Schleichen 3,
Herbeirufen 6, Verbannen 8, Maskieren 2  
Zauber: Levitation, Panzerung, Flammenwerfer, Reflexe steigern, Heilen, Manaball


