# Ablauf

1. Runner:in findet heraus, dass Hidden Justice Ltd. Technomancer umbringt, Auftrag kommt von der Abteilung
2. Zusammen mit Damsel Hack auf die Abteilung; dabei stößt man auf Bücherwurm. Gemeinsamer Tiefenhack gegen die Abteilung, um Consensus' Backup zu löschen; danach Kampf gegen Consensus
3. Zusammen mit Bücherwurm Hack auf den Bund, um herauszufinden was sie wissen, sie zu exposen, und Machenschaften zu beenden

Der Bund
- Geheimgesellschaft von hochrangigen Decker:innen/Spinnen, die für i-welche mächtigen Execs arbeiten.
- Zweck: Wissen über Technomancer, um mehr über die Matrix zu verstehen, und damit sie einem nicht den Rang ablaufen.
- Technos und KIs gegenüber eher feindlich gesinnt
- Versuchen Kons in eine Richtung zu manipulieren, die der Matrix mehr Macht gibt, aber komplexer macht, und ihre Position stärkt.
- Aufbau:
  - Hierarchie durch Mystik. Wissen wird vom Archivar verwaltet
  - Neuer Archivar wird, wer die Identität des alten herausfindet
  - Archivar gibt den anderen Mitgliedern Aufgaben; als Belohnung erhalten sie Wissen, das jmd anders gefunden hat
  - man kann mehrere Ränge aufsteigen, damit es immer jmd gibt, auf den man herabgucken kann

Sucherin
- Seeka arbeitet für Hidden Justice, ist aber auch beim Bund.
- man begegnet ihr kurz in #1 im Hidden Justice-Host, wo man sie evtl kurz belauscht und sie eins entdecken kann?
- Im 3. Teil kann man ihr Informationen abluchsen

Bücherwurm
- Niedriger Rang im Bund, soll während #2 Abteilung hacken
- aber nachdem er von Verbrechen des Bundes erfährt, hilft er Runner:innen dabei, den Bund zu infiltrieren

Damsel
- In #1 erfährt man, dass sie auf der Abschussliste steht
- wenn man sie warnt, will sie helfen, die Abteilung platt zu machen

Archivar
- Angst vor Technomancern
- Persönliche Sicherheitsspinne von hohem SK-Exec
- Hat Horizon manipuliert um Technomancer-Massaker zu starten
- Will an das geheime Wissen von "der Abteilung"

