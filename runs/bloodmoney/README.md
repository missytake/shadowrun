# Blood Money

Zwei AG Chemie-Konzernbengel sind dumm genug, für eine Mutprobe in eine
Infizierten-Bar zu gehen. Dort werden sie von der Gang der "Living Dead"
gekidnappt. Die wollen nun Lösegeld.

[NPC-Werte](npcs.md)

## Hintergrund

Tapirs Cousin Sandro Mauer ist bei der AG Chemie und arbeitet in einem Labor
für Pestizide. Sein Kollege Anton Duskin ist bei der Innenrevision. Ihre
Kollegin Natalie Medved, die in der Abteilung für Pestizide arbeitet, hat sie
zu einer Mutprobe provoziert - sie sollen einen Dienstagabend im Xanhaem's
verbringen. Wer länger durchhält, darf mit ihr ausgehen.

Natalies Boss hat Angst vor den Testergebnissen von Sandros Abteilung, und
Angst vor der Innenrevision - denn die testen u.a. die Pestizide ihrer
Abteilung auf Umweltschädlichkeit. Mit Insider-Infos will sie sich bei ihrem
Boss einschleimen. Also hat sie die Gang "Living Dead", die im Xanhaem's
Türsteher sind, beauftragt, die Kommlinks der beiden zu beschaffen.

Die Gang will jedoch doppelt kassieren, und hat die beiden gekidnappt, um sie
als Geiseln zu verkaufen. Sie überschätzen dabei deutlich deren Wert, und
wollen 20.000 pro Person. Der Ghul "Kiefer" wartet mit deren Kommlinks darauf,
dass jemand sie sucht, um ihnen das Angebot zu unterbreiten. Er kann allerdings
wegen dem Fingerabdrucksensor keine Anrufe annehmen (und weil er blind ist).

Die Gang hat die beiden Konzernbengel bei "Klaue" untergebracht, einem Ghul
und Leutnant der Living Dead. In seiner Bude hängen immer 4-5 Ganger herum,
tagsüber nur nicht-infizierte natürlich.

Gegen Mittwoch Vormittag schreibt Tapirs Tante aufgelöst in die Familiengruppe,
dass Sandro nicht heimgekommen ist. Und dass er im Rhein-Ruhr-Plex feiern
gehen wollte.

Donnerstag Mittag um 12:00 will sich Natalie auf dem Platz vor dem Kölner Dom
mit "Messer" treffen, einem nicht-Infizierten Leutnant der Living Dead, um die
Kommlinks gegen Geld einzutauschen.

## Tante Schmidt

Mittwoch vormittag, Tapirs Kommlink blinkt. Ihre Tante, Klara Mauer schreibt:

"Hey meine Lieben, hat sich Sandro zufällig heute bei einem von euch gemeldet?
Er ist gestern nicht heimgekommen. Klar, er ist fast 20 und groß genug, grade
für einen Ork. Aber ein bisschen Sorgen mache ich mir schon."

(Pandora, wann stehst du ca. auf abends?)

Eine halbe Stunde später schreibt sie:

"Bei der AG ist er heute auch nicht zur Arbeit aufgetaucht. Sein Kollege Anton
fehlt auch. Die beiden wollten wohl gestern im Rhein-Ruhr-Plex feiern gehen..."

## Beinarbeit

Sandro Mauer (Fakebook-Profil)
- 0: Ork, 19 Jahre alt, arbeitet bei der AG Chemie. Scheint sein Gehalt in
  modische, aber nichtssagende Kleidung zu stecken
- 1: Selfie mit #Mutprobe: Gestern Abend war er mit seinem Kumpel Anton Duskin
  feiern im Rhein-Ruhr-Megaplex.
- 3: Sandro macht bei der AG Chemie eine Ausbildung zum Laborassistenten.
- 4: Bei Sandros Arbeit dreht es sich um das Testen von Schädlichkeit von
  Pestiziden.

Anton Duskin (Fakebook)
- 0: Mensch, 21 Jahre alt, arbeitet bei der AG Chemie. Gothic-Style
- 2: Anton ist Rechnungsprüfer bei der AG Chemie, Innenrevision
- 3: Keine Familie mehr, Eltern bei Autounfall umgekommen

Anton Duskin (Finstergram)
- 4: Ein Selfie mit Lederkutte und bleich geschminktem Geischt: "Heute traue
  ich mich endlich ins Xanhaem's. Alles was ich gebraucht habe, war ein
  Arschtritt. Ich hoffe, mit diesem Outfit falle ich nicht unter 'Beute'."

Xanhaem's (Tipp-Advisor, Straßenwissen Rhein-Ruhr-Plex)
- 0: Ein Gothic-Club im Rhein-Ruhr-Plex, inkl. Koordinaten. Hat keinen eigenen
  Matrix-Auftritt. 10 Nuyen Eintritt.
- 2: Im Xanhaem's sind MMVV-Infizierte geduldet, bzw. zeigen sich sehr offen.
- 3: Dieser Teil der Brache wird "Ghoulies Ground" genannt, die Türsteher sind
  von der Gang "Living Dead". Waffen sind erlaubt.
- 4: Die Barkeeperin heißt Gerri und ist Tiger-Gestaltwandlerin. Sie kennt wie
  alle Stammgäste und vermittelt manchmal Jobs.

Living Dead (Straßenwissen Rhein-Ruhr-Plex. Im Xanhaem's rumfragen ist
einfacher, Erfolge weniger nötig)
- 1: Eine Gang in der Brache des Rhein-Ruhr-Plexes. Ein Großteil der Ganger
  sind Ghule.
- 2: Wenn man was von den Living Dead will, sollte man im Xanhaem's nach Kiefer
  fragen. Die drei Leutnants heißen Klaue, Kiefer, und Messer.
- 3: Weil Ghule blind sind, haben nur die Metamenschen in der Gang Kommlinks.
- 4: Ihr Hauptquartier ist in einem Plattenbau, nicht weit vom Xanhaem's. Sie
  verlangen Schutzgeld von den Leuten, die da wohnen.

Kiefer (Straßenwissen Rhein-Ruhr-Plex, evtl. Connections)
- 2: Ein Ghul, Leutnant der Living Dead. Sozusagen Sprecher der Gang. Wenn man
  was von den Livin Dead will, sollte man im Xanhaem's nach ihm fragen.
- 3: Seine Bude ist in Zimmer 303. Ist so ein bisschen das Büro der Gang.
- 4: Hat ein bisschen Angst vor Klaue, und dass der eines Tages auf die Idee
  kommt, ihn nicht mehr zu brauchen. Aber macht Cram nicht alle paranoid?

Klaue (Straßenwissen Rhein-Ruhr-Plex, evtl. Connections)
- 2: Ein Ghul, Leutnant der Living Dead. Kümmert sich um die hässliche Seite
  ihrer Geschäfte. Dass er ständig auf Cram ist, hilft nicht gerade.
- 4: Seine Bude ist in Zimmer 606. Manchmal hört man von da Schreie.

Messer (Straßenwissen Rhein-Ruhr-Plex, evtl. Connections)
- 2: Troll, Leutnant der Living Dead. Kümmert sich ums Tagesgeschäft (ha ha).
- 3: Komischer Humor. Obwohl er sich Messer nennt, ist er eigentlich nur mit
  Schusswaffen gut.
- 4: Seine Bude ist in Zimmer 203. Er schläft Nachts und übernimmt tagsüber die
  Aufgaben der Gang.

Natalie Medved (Fakebook-Profil)
- 0: Mensch, 24 Jahre alt, arbeitet bei der AG Chemie. Profilbild professionell
  mit Blazer
- 2: Natalie arbeitet als Projektmanagerin bei der AG Chemie. Abteilung
  Pestizide.
- 4: Karriereknick vor einem halben Jahr. War vorher für Medikamente zuständig,
  da gab es aber Probleme mit Nebenwirkungen.

## Im Xanhaem's

Avanya ist bereits im Xanhaem's, weil Mittwoch abend einfach eine gute
Gelegenheit für sowas ist.
- Bühne mit düsterer & aggressiver Live-Musik - Metal mit synth-Elementen
- große Tanzfläche, an der Seite Tische und die Bar
- Hinten ein VIP-Bereich, leicht erhöht. Ein Ghul-Ganger steht davor (dort
  sitzt Kiefer, mit 2 Gangern)

Wenn Tapir Sandro einfach anruft, klingelt das Kommlink bei Kiefer im
VIP-Bereich. Er zieht es aus der Tasche, schaut irritiert drauf, versucht drauf
rumzudrücken, gibt es dem Ork hinter sich, der routiniert auflegt und es ihm
zurückgibt.

Die Barkeeperin Gerri ist blond, was kaum in die Grufti-Athmosphäre passt, und
abwechselnd extrem unnahbar oder extrem aggressiv. Wenn man nach den AG
Chemie-Jungs von Dienstag fragt (Avanya hat es etwas leichter):
- 0-2: Keine Ahnung, erinnere mich nicht
- 3-4: Bei solchen Sachen solltet ihr Kiefer fragen - hinten im VIP-Bereich
- ab 5: Diese Poser sind gestern mit den Living Dead hier aneinandergeraten und
  wurden raus gezerrt. Wahrscheinlich sind sie jetzt bei Klaue im HQ.

Nachdem ihr sagt, dass ihr zu Kiefer wollt, werdet ihr in den VIP-Bereich
gelassen. Kiefer sitzt an einem Tisch, hinter ihm stehen zwei Ganger - ein
Ghul, ein Ork. 
- Mussten lernen, dass i-welche Konzernbengel nicht so gut hierher passen
- Die AG Chemie lässt doch bestimmt was springen, und sonst hat eure Familie
  doch sicher was gespart
- 20.000 Nuyen pro Geisel, bis morgen Nacht. Sonst sind sie Nahrung
- Er lässt sich mit einer vergleichenden Probe auf Verhandlung auf 10.000 pro
  Person herunterhandeln.
- Bei 4 Netto-Erfolgen auf 5.000 pro Person.

## Beim Plattenbau

### Zimmer 203

Messer schläft in seinem übergroßen Trollbett. Immer abends & morgens trifft er
sich mit Kiefer & Klaue, um die Schicht zu übergeben. Die Fenster haben normale
Vorhänge.

### Zimmer 303

Die Fenster sind verrammelt - hier schläft Kiefer tagsüber, nachts hängt er im
Xanhaem's rum. Das Wohnzimmer ist Verhandlungszimmer, mit einem tiefgelegten
Fließentisch und Sofas. Von da gehen Schlafzimmer und Küche ab. In der Küche
lagern Leichenteile im Kühlschrank.

### Zimmer 606

Hier gibt es ein Wohnzimmer, in dem einige Sofas rumstehen, und 4 Ganger
herumhängen und relativ leise Goblin Rock hören (Ein Kompromiss zwischen dem
verbesserten Gehör der Ghule und dem Musikgeschmack der Orks). Hinter einer
Kommode können sich zwei Leute verschanzen. Von dem Wohnzimmer gehen zwei
Schlafzimmer und eine Küche ab. Nachts hängt Klaue abwechselnd in Küche &
Wohnzimmer ab.

In der Küche gibt es eine Art Käfig am Ende des Ganges. Das Schloss ist
mechanisch (Stufe 2), Klaue hat den Schlüssel. Außerdem gibt es einen
Kühlschrank mit Leichenteilen. Klaue hat sehr viel Spaß daran, sich vor seinen
Gefangenen Essen zuzubereiten und sich an ihrer Todesangst zu ergötzen.

Im Schlafzimmer schlafen Nachts zwei Ganger. Tagsüber schläft Klaue hier.

## Die Übergabe

### Beschattung Messer

Gegen Ende der Nacht fährt Messer ins Xanhaem's, um sich mit Kiefer
abzusprechen. Der gibt ihm die Kommlinks für Natalie. Daraufhin fährt Messer
erst zu einem Mechaniker in Düsseldorf, um ein Backup der Kommlinks zu ziehen.
Dort bleibt er bis ca. 11, bis er sich auf den Weg zum Kölner Dom macht.

Vor dem Kölner Dom wartet er 10 Minuten. Um Punkt 12 wird er von einer jungen
Frau mit Sonnenbrille, Atemschutzmaske, und Mantel angesprochen. Er gibt ihr
die beiden Kommlinks und kriegt dafür einen unbeglaubigten Credstick über 5000
Nuyen.

Danach fährt er zurück in den Plattenbau und klingelt Mieter:innen durch, die
im Verzug sind. Abends kommt er ins Xanhaem's, erstattet Kiefer Bericht, gibt
ihm den Credstick, und fährt nach Hause, schlafen.

### Beschattung Natalie

Natalie hat ihr Auto (Hyundai Shin-Yung) in einem Parkhaus in der Nähe geparkt.
Damit fährt sie zurück nach Groß-Frankfurt zu einem Techniker, der die
Kommlinks analysieren soll. Danach fährt sie nach Hause in Frankfurt-Hoechst.

## Die Bezahlung

Donnerstag Nacht ist Kiefer wieder im Xanhaem's. Vorher schaut er bei Klaue
vorbei und vergewissert sich, dass alles in Ordnung ist, und die Geiseln noch
leben.

Wenn man ihn auf die Geiseln anspricht:
- Ja, 20.000 pro Person. Heute ist die letzte Gelegenheit, übermorgen würden
  sie nicht mehr schmecken.
- Er lässt sich mit einer vergleichenden Probe auf Verhandlung auf 10.000 pro
  Person herunterhandeln.
- Bei 4 Netto-Erfolgen auf 5.000 pro Person.

Wenn die Runner zahlen können, nimmt er sie mit zum Plattenbau. Klaue und seine
Leute kommen raus und bringen die beiden Geiseln mit. Kiefer verlangt nach dem
Credstick und checkt wieviel drauf ist. Wenn alles passt, lassen sie die beiden
frei. Wenn nicht, eröffnen sie das Feuer auf euch; die Geiseln töten sie
zuletzt.

## Belohnung

Die AG Chemie ist bereit 5.000 Nuyen pro Rettung zu zahlen, wenn man den Chef
der beiden vorher fragt. Die Familie Mauer kann auch noch mal 3.800 Nuyen
zusammenschmeißen.

Karma-Belohnung:
- Rettung Sandro: 4 Karma
- Rettung Anton: 2 Karma
- Natalie aufgespürt: 2 Karma
- Alle LD-Leutnants getötet: 2 Karma
- Niemanden getötet: 2 Karma
- Kampflos gelöst: 2 Karma

