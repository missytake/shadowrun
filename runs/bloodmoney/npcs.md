## Klaue / Kiefer

![Ghul-Werte](ghul.png)

```
Kein Kommlink, Panzerung 12, Kampfaxt: Präz. 4, S+5K, DK -4
Fertigkeiten: Gebräuche (Straße) 6/7, Verhandeln 5/6, Gewehre 5/8, Waffenloser Kampf 6/9, Klingenwaffen 6/9
Standardmäßig auf Cram: +1 Reaktion, +1d6 Initiative
```

## Messer (Cyberware + Schießen)

```
K	G	R	S	W	L	I	C	Ess
6	7	7	6	4	2	3	3	1,9
Initiative 12+3d6     Limits: K9 G4 S4
Panzerung 12	Ausweichen 10	Zustandsmonitor 11/10
Ares Predator V (Pistole): Präz. 5, 8K, DK -1, HM, 15(m)
Ingram Smartgun X (MP, Schalldämpfer): Präz. 4, 8K, SM/AM, RK 2, 32(s)
```
**Fertigkeiten:** Gebräuche (Straße) 3/6, Pistolen 4/11, Schnellfeuerwaffen 4/11
**Ausrüstung:** 1 Dosis Cram, Kommlink Stufe 3, Panzerweste (9)

Harley-Davidson Scorpion: Handling 4, Geschw 4, Beschl 2, Rumpf 8, Panzerung 9

## Ghul-Ganger

![Ghul-Werte](ghul.png)

```
kurzläufige Defiance T-250 (Schrotflinte): Präz. 4, 9K, DK -1, EM, 5(m)
Fertigkeiten: Gebräuche (Straße) 3/4, Gewehre 5/8, Waffenloser Kampf 6/9
Zustandsmonitor 12, Panzerung 9, Kommlink Stufe 1
```

## Ork-Ganger

```
K	G	R	S	W	L	I	C	Ess
6	4	3	6	3	2	3	3	6
Initiative 6+1d6     Limits: K5 G4 S5
Panzerung 9	Ausweichen 6	Zustandsmonitor 11
Browning Ultra-Power (Pistole): Präz. 5, 8K, DK -1, HM, 10(m)
Messer: Präz. 5, 5K, DK -1
```
**Fertigkeiten:** Gebräuche (Straße) 3/6, Klingenwaffen 4/8, Knüppel 3/7, Pistolen 4/8, Waffenloser Kampf 3/7  
**Ausrüstung:** 1 Dosis Cram, Kommlink Stufe 1, Panzerweste (9)

