# Flappflight

Das Ziel dieses Runs ist das Helikopter-Startup Flappflight. Der CEO, Jacob
Leupold, war früher Ingenieur bei Saeder-Krupp, ist aber bei SK rausgeflogen.
Danach hat er FlappFlight aufgemacht, um zu beweisen, dass er bessere Helis
bauen kann als SK.

Als SK feststellte, dass sein erster Prototyp tatsächlich besser ist als was SK
anbietet, wollten sie den Laden kaufen. Er will aber nicht mehr für SK
arbeiten und hat abgelehnt. Deswegen sollen die Runner den Prototyp und die
Baupläne klauen.

## Treffen mit Frau Schmidt

Das Fischhaus "Unsere Frau am Hafen" ist ein etwas abseits gelegenes Hamburger
Fischrestaurant in Altona, an der Grenze zu Pinneberg, das schon etliche Jahre
existiert. 

Die Küche ist hervorragend, es werden praktisch nur natürliche Zutaten
verwendet. Die Fische, Meeresfrüchte und sonstigen Zutaten sind
selbstverständlich nicht aus der verseuchten Nordsee, Elbe oder Alster, sondern
aus kontrollierter Aquakultur. 

Die Athmosphäre im Lokal ist ruhig und gediegen. Ihr seht natürlich Securitys,
aber sie stehen höflich und zurückhaltend an der Seite. 

Der Kellner geleitet euch zu einem Separeé, das sich weiter hinten in dem
Restaurant befindet. Frau Schmidt sitzt schon da und wartet auf euch, ihre
Bluse hoch zugeknöpft und mit einem entwaffnenden Lächeln. "Ah, da seid ihr
ja. Setzt euch. Wieviel wisst ihr bereits?"

"Genau, die Sache ist an sich einfach. Eine Fabrikhalle in
Hamburg-Kaltenkirchen, in der Nähe des Flughafens. Das war mal von so Chaoten
besetzt, die haben da Konzerte und sowas veranstaltet. 

Jetzt ist da drin ein Startup, das Helikopter und Drohnen fertigt. Ziemlich
naive Jungs, aber einer ihrer Helikopter-Prototypen scheint tatsächlich gute
Arbeit zu sein. Wir brauchen den Prototypen und die Blaupausen, um ihn
nachbauen zu können. 

Ist wirklich dürftig bewacht, wenn ihr nachts reingeht, solltet ihr nahezu
freie Bahn haben. Wenn ihr ihn und die Daten habt, fliegt ihr ihn zu einem Wald
in der Nähe von Bad Godesberg, die Koordinaten folgen noch. Am besten löst ihr
keinen Alarm aus, ihr wollt ja nicht die Flugsicherung hinter euch her haben.
Da kriegt ihr dann 50k Nuyen, insgesamt."

Schmidt lässt sich mit 2 Erfolgen im Verhandeln auf 60k hochhandeln.

## Beinarbeit

Flappflight
* 0 Erfolge: Keine Ahnung, kenne ich nicht. Noch nie gehört.
* 1 Erfolg: Ja, das ist so ein kleines Startup, relativ neu. Machen echt ganz
  gute Drohnen.
* 2 Erfolge: Der Typ, der das aufgemacht hat, ist nicht mal so jung. Schon über
  40. Ungewöhnlich für ein Startup. Jacob Leupold heißt er.
* 3 Erfolge: Der CEO ist gleichzeitig der Chefingenieur. Ist so ne 1-man-show.

Jacob Leupold
* 1 Erfolg: Den Namen habe ich neulich mal in der Werbung gehört. Flappflight?
* 2 Erfolge: Der war vorher bei Saeder-Krupp. Hätten ihn wohl nicht gehen
  lassen sollen, jetzt haben sie plötzlich nen starken Konkurrenten.

Fabrikhalle
* 1 Erfolg: Hm, ich vermisse die alten Tage. Da gab es da echt geile Konzerte.
  Etwas abgelegen, aber umso lauter konnten wir feiern.
* 2 Erfolge: Da sind nachts wohl nur zwei Wachleute. Jedenfalls stehen nur zwei
  Autos vor der Halle. Na gut, wer weiß, was da sonst noch wartet.
* 3 Erfolge: Hier ist der Grundriss der Fabrik. Hat nur ein Stockwerk. War
  nicht einfach, das aufzutreiben, hoffe, das hilft.

## Fabrikhalle

Fabrik: 1 Fabrikraum mit aufmachbarem Dach, 1 Serverraum, 1 Security-Room mit
Kamerascreens & Knopf fürs Dach. Die Lichter sind Neon-Röhren mit UV.

War mal Besetzt & Konzerthalle, wurde geräumt, damit so ein Startup wieder ne
Fabrik reinbauen kann. alles voller Grafittis, die die zu faul waren wegzumachen

### Security

2 Wachen watscheln zwischen Fabrikraum, Serverraum, Security-Room hin und her.
Ihre Autos stehen vor der Tür. Einer der beiden Wachen hat einen
DocWagon-Standard-Vertrag - wenn man ihn ausschaltet, ist ein HTR-Team
unterwegs.
* 40 Jahre alt, Familienvater, DocWagon-Standard-Vertrag
* 22 Jahre alt, Skinhead, aggressiv und schießwütig
Die Werte sind die von Konzernsicherheit:
(GRW 382):
* Kommlink Renraku Sensei: Stufe 3
* Initiative: 7+1d6
* Dodge: 7
* Soak: 14
* Zustandsmonitor: 10
* Hit: 7
* Fichetti Security 600: 7K dmg
* Stun Baton: 9G dmg, DK -5

Im Fabrikraum stehen auch ein paar bewaffnete Rotorrohnen und Watchman-Drohnen
rum, die über die Matrix aktiviert werden können
* Rotordrohnen (Flappflight Wasp): können keine Türen öffnen - einfach zu
  entkommen
* Watchman: weniger Feuerkraft

Im Serverraum und im Security-Room stehen auch jeweils 1 schlafender Watchman.

Die Wasp-Drohnen sind auf Standby, und müssen über die Matrix aufgeweckt
werden. Die Watchman-Drohnen reagieren auf Schussgeräusche, alarmieren die
Wasp-Drohnen aber erst, wenn sie tatsächlich Runner sehen. Die Wachleute haben
jeweils 2 Marken auf ihnen, um sie zu steuern.

Die Vordertür ist mit einem Magschloss Stufe 2 geschützt.

Stufe 4 Luftgeist als magische Abwehr
* Wenn er nicht ausgeschaltet wird, folgt er dem Heli und erstattet Bericht.

Stufe 4 6457 Host für Firmengeheimnisse
* IC
  * Aufspüren 
  * Killer
  * Wirbel
* Dateien
  * Secr3t-Krupp (Stufe 3)
  * FF1_Blueprints (Stufe 2)
* Geräte
  * Storage Server (Stufe 3)
 
Stufe 3 4365 Host für die Fabrik + Sicherheitssystem 1 Marke
* IC
  * Patrouille
  * Killer 
  * Marker
* Dateien
  * Kamera-Aufzeichnungen
* Geräte
  * Kameras (stufe 3)
  * Dachluke (stufe 1)
  * 4 Watchman-Drohnen (auf Schleichfahrt) (stufe 5)
  * 4 Wasp-Drohnen   (auf Schleichfahrt) (stufe 4)
  * Tor in der Mauer (stufe 1)
  * Magschloss (Stufe 2)
  * Bildschirm der Überwachungskameras (Stufe 2)

### Sags ihnen ins Gesicht

Als sich der Hamburger Sprawl Stück für Stück ausbreitete, und sich die
umliegenden Orte einverleibte, wurde auch Kaltenkirchen Teil von Hamburg.
(Ankunft in Kaltenkirchen)

Fabrikraum

Sicherheitsraum

Serverraum

## Flucht

Wenn die Runner keinen Alarm ausgelöst haben, werden sie auch nicht verfolgt.
Wenn doch, sind zwei Helis von HanSec hinter ihnen her, die aber nicht so viel
Geschwindigkeit auf den Tacho bringen.

### Sags ihnen ins Gesicht

Helis hinter euch her

Landung + Diskussion mit Schmidt

