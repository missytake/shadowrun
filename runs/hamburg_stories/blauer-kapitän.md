# Der Blaue Kapitän

    • Lobatchevski-Vory wollen in Bergedorf Präsenz zeigen, um Dealer unter ihre Kontrolle zu kriegen, und sich den Markt zu sichern. Sie gehen in Clubs und machen einen auf hart, drohen und erschießen/verkrüppeln vllt jemanden.
    • Yakuza hat nur wenige ausführende Assets vor Ort und kann sich keine Konfrontation leisten, will deswegen nur, dass sie lächerlich gemacht werden. 
    • "lasst sie dumm aussehen. Sodass niemand vor ihnen Angst hat. macht ihre Einschüchterungsversuche wirkungslos."

## Ablauf
    • Herr Schmidt
    • Recherche
    • Club im blauen Klotz in Harburg
    • Vorys betreten den Raum, machen eine Szene
    • Auseinandersetzung

## Szene 1 - Schmidt

    • Fixer gibt euch Adresse zu Host in der Matrix, ein Chatroom.
    • Host ist bis auf euch leer, einfarbig graue Wand, keinerlei Hinweise. Keine Türen.
    • Avatar des Schmidt materialisiert sich: Mann im Anzug ohne Gesicht, feine Animationen, digitale Computerstimme. Text ist anscheinend automatisch übersetzt.

Ihr seid die vier Runner, die mir empfohlen wurden? Seid ihr schon in Hamburg? 

In Hamburg-Harburg kontrolliert die Vory den Drogenhandel. Schon mal mit ihnen aneinandergeraten? Hat jemand von euch Verbindungen zu ihnen?

    • Es gibt diesen Club, den "blauen Kapitän". 
    • Im 11. Stock des blauen Klotzes, der größte Schwarzmarkt in Hamburg. 
    • Dort verkaufen bisher alle möglichen Leute einfach frei und unbehelligt ihre Drogen. 
    • Die Vory will alle freischaffenden Dealer aus dem "Kapitän" vertreiben, die nicht ihr minderwertiges Zeug verkaufen. 
    • Sie schrecken auch vor extremer Gewalt nicht zurück.
    • Ziel: Gewalt der Vory soll enden, auch keine Vergeltungsschläge.
    • Lasst sie dumm aussehen, kreativ scheitern. So, dass niemand vor ihnen Angst hat. macht ihre Einschüchterungsversuche wirkungslos, dann werden sie damit aufhören und die Leute in Ruhe lassen. Macht sie irgendwie lächerlich, aber vermeidet Gewalt. 
    • Niemand will, dass die Lage weiter eskaliert. Die Sprache der Gewalt sprechen die besser als wir.
    • Wenn ihr es schafft, ihre Vorherrschaft in Frage zu stellen, ohne dass es zu einer gewalttätigen Eskalation kommt, kriegt ihr insgesamt 10k pro Person. 
    • Wenn ihr sie nur mit Gewalt daran hindern könnt, die Leute zu verjagen, nur die Hälfte.

## Beinarbeit

Vory
    • Ja, die haben Harburg praktisch unter Kontrolle. Kein Wunder, dass das Viertel so am Arsch ist.
    • Seit die hier das Sagen haben, misstraut nur noch jeder jedem. Alle haben Angst vor "der Krone". Die Geschäfte kommen zum erliegen, kein Wettbewerb, keine Innovation. das Zeug wird immer schlechter.
    • Eigentlich machen die nur einen auf dicke Eier, und alle wissen das. Keine Ahnung, warum noch jemand das Gehabe ernst nimmt. Das meiste sind nur leere Drohungen, und sie können ja nicht jeden kaltmachen. "Die Krone" treibt für sie das Geld ein und schüchtert alle ein.

Blauer Klotz
    • Adresse
    • ziemlich unübersichtlich. Nichtmal die Vory könnte darüber den Überblick behalten, und die hat hier ja sonst alles unter Kontrolle. Und die AR ist so zugespammt dort, heilige Scheiße
    • Der Barkeeper im "blauen Kapitän" ist ein netter Kerl. Weiß nicht, wieviel Bock der auf die Vory hat, aber er kann da halt auch nicht viel ausrichten.

Yakuza
    • Die Yakuza haben nicht so viel zu sagen hier, fernab von Japan.
    • Hätten wohl gerne einen Fuß in der Tür des Drogenmarkts, aber die Vory weiß das wohl zu verhindern.

Drogenmarkt
    • Die Vory und ihre Großmachtsfantasien... am liebsten hätten sie ein Monopol.
    • Der "Blaue Kapitän" ist definitiv der beste Markt im Moment - auch deswegen, weil nicht nur die Vory sich dort trauen zu verkaufen. Der Barkeeper bemüht sich, seinen Laden für alle offen zu halten.
    • Die Vory haben ihre Monopolstellung nur, weil alle Leute Angst vor ihren dicken Eiern haben. Typisch fragile Männlichkeit - Stärke ist verbunden mit überzogener Grausamkeit, Heterosexualität und Fleischkonsum. Wenn man ihnen das nimmt, spielt niemand mehr bei ihrer Vormachtsstellung mit.

Die Krone
    • Seltsamer Typ. macht mir Angst. der hat gar kein Gewissen... Ich habe gehört, er war mal Zahnarzt, bis etwas schreckliches passiert ist. was? keine Ahnung.
    • Ich glaube, er war früher ein ganz normaler Zahnarzt, ordentlicher Bürger - bis irgendetwas in ihm zerbrochen ist. was hat er wohl angestellt?
    • Eigentlich ein armer Tropf. Er hatte mal eine Tochter, der ist dann mal etwas zugestoßen... Seitdem ist er kein Zahnarzt mehr, sondern macht Leute fertig.

## Der Blaue Klotz
    • leuchtend blau gestrichenes Hochhaus 
    • 14 Stockwerke
    • mitten im Slum-Bezirk des Hamburger Stadtteils Harburg.  
    • Drum rum zerfallende Plattenbauten, verdrekte Spielplätze, ehemalige Grünanlagen mit gelbbraunem Gras. Die Bäume sind längst am sauren Regen krepiert
    • ein einziger, riesiger Schwarzmarkt.
    • Früher kleine Wohnungen – heute Geschäfte. 
    • Dönerbuden, chinesische Garküchen, Schuhmacher, Waffenläden, Sweatshops, wo  illegale Einwanderinnen für einen Hungerlohn Flickarbeiten an Textilien vornehmen, Trödler, Elektronikbastler-Shops und Büchsenmacher-Läden, bis hin zu Drogenküchen oder Taliskrämer.
    • Prostituierte gehen hier ebenso ihrem Gewerbe nach, wie Tattoo-Studios und der eine oder andere Straßendoc, die ihre Dienstleistungen anbieten.
    • Hier und dort sieht man als AR-Grafitti das Logo der Vory y Zakone.
    • Aufzug aus den zwanziger Jahren. Neben 11. Stock ein Schild “Blauer Kapitän”

### Der Blaue Kapitän
    • Luft im 11. Stock schlimmer als im Rest
    • Zigarettenqualm, der Rauch von Shishas, Geruch nach Lösungsmittel
    • Zwischenwände wurden herausgerissen, um die kleinen Wohnungen zu einem größeren Raum zu verbinden
    • Raum in der AR völlig schmucklos. Keine Vory-AROs, aber auch sonst nicht viel. 
    • Selbst außerhalb der AR ist aus den Boxen ein stakkatoartiger Electron Wave zu hören.
    • Teilweise Graffitis gegen den Staat an den Wänden
    • Manche sitzen gemeinsam an kleinen Tischen, schnacken und trinken
    • Einzelne lehnen an der Wand oder liegen auf alten Sofas, benommen / bewusstlos
    • 4 Metamenschen sitzen jeweils einzeln herum, ein paar Tütchen oder Chips vor sich. Sie tragen ausnahmslos weite Mäntel, und gucken verstohlen herum. Ein Asiatisch aussehender Zwerg, eine menschliche Frau, ein menschlicher Mann, und ein Ork.
    • An einer längeren Bar steht ein Barkeeper und putzt ein Glas.

### Barkeeper:
    • Ist von den Vory genervt
    • Ich mag ja meine Kunden! warum sollen sie nicht einfach kaufen, bei wem sie wollen?"
    • Vory kommt manchmal, macht Leuten Angst, macht Körperteile kaputt.
    • Vory verkauft schlechtes Zeug.
    • Kein Monopol wäre am schönsten. Kunden sollen selbst entscheiden ob Yakuza, Vory oder bei den gottverdammten Cops.
    • Viele boykottieren Vory, nur noch open-source-BTLs. Schlechter, aber sicherer
    • Johann Mager / "die Krone" ist der schlimmste. vertreibt immer jeden, der nicht für die Vory dealt. Abgestumpfter, grausamer Typ.
    • Vory hat Problem mit selbstständigen Kleindealern, weil die von ihren Konkurrenten kaufen. Die Dealer der Vory verkaufen gar nichts anderes, ein Kartell.
    • Freischaffende kaufen vor allem bei Yakuza

## Auftritt: Vory

    • Aufzug klingelt. Vier Vorys treten aus dem Fahrstuhl. 
    • Der Anführer ist ein blonder Elf mit einer AR-Brille. ungewöhnlich breites Gesicht, kann nicht aufhören zu grinsen.
        ◦ Er wird von einem großen Troll begleitet, knackt immer wieder mit den Fingern.
        ◦ Ein groß und böse aussehender Ork 
        ◦ eine stark vercyberte Frau mit gelangweilter Miene.
    • Musik verstummt. alle Köpfe drehen sich zu ihnen hin. 
    • Die Dealer schaufeln hastig die Substanzen in ihre Taschen.

    • "Verehrte Damen und Herren", der Blonde ist höflich & kalt.
    • Was für ein schöner Laden! Ist doch schön genug, wenn hier nur die Vory verkauft.
    • Sind ja hier immer noch in Harburg.
    • Hat mir jemand von euch etwas zu beichten?

    • Sie gehen zu dem asiatisch aussehenden Zwerg hin, sitzt mit Rücken zu ihnen. 
    • Er versucht, unauffällig auszusehen, hat aber ein riesiges Tattoo über der Glatze.
    • Krone: "Die Yakuza ist zum Beispiel nicht die Vory, sondern die Konkurrenz."
    • Der tätowierte Dealer versenkt die Hände bis zu den Ellenbogen in den weiten Taschen seines Mantels, presst die Arme an die Seite.
    • "Und wenn nur die Vory in Harburg verkaufen darf, dann darf die Yakuza offensichtlich, na?"
    • Der Blonde setzt sich dem Dealer direkt gegenüber, stützt sein Kinn in Hände und schaut ihn erwartungsvoll an. Der stottert: "... nicht verkaufen?"
    • "Richtig geraten!" Er schnipst direkt vor dem Gesicht des Dealers mit den Fingern. Der erschreckt und will aufspringen, doch der Troll hinter ihm drückt sofort seine Schultern wieder herunter. Der Rest des Clubs schaut immer noch gebannt zu.
    • "Meinst du, du brauchst noch all deine Finger? Gibt doch heute überall Ersatzteile."
    • Krone zieht ein Skalpell aus der Innentasche seines Anzugs. 
    • "Seit die Krone keine Zähne mehr zieht, zieht sie eben lieber Finger!"

    • Dealer lässt seinen abgetrennten Finger an Ort und Stelle liegen, rennt zum Aufzug 
    • Krone steht gelangweilt wieder auf. 
    • "Alle anderen, die nicht für die Vory arbeiten, haben zwei Minuten, um ihren Arsch hier rauszubewegen!"
    • Krone geht zur Theke, bestellt sich einen Drink.
    • Seine drei Gefolgsleute lassen ihre Blicke durch den Raum streifen
    • Dealer packen langsam ihr Zeug ein und gehen.

    • Krone wird bleich, als überall in der AR die belastenden Bilder auftauchen.
    • Sein Mund öffnet sich vor Schreck, und sein Blick ist an das Foto seiner Tochter gerichtet, dass überall zu sehen ist.
    • Seine Gang schaut ihn ratlos an. Sie merken schon, dass es nicht so ganz nach Plan läuft…
    • Zwergin auf Speed, steht plötzlich auf und schlägt mit Tasche auf Krone ein. 
    • "Du Monster! Wie kaputt muss man eigentlich sein! Ihr Vorys seid so daneben!"
    • Orks in der Ecke fangen an, sie auszulachen. Hilflos zieht der Ork eine Ares Predator V, nur um sie wieder wegzustecken.
    • Der Troll greift Krone um die Schultern und zieht ihn in Richtung Aufzug. Unter allseitigem Gelächter und Buhrufen verlassen die Ganger das Stockwerk.


## Schmidt

    • Host ist genauso grau wie vorher
    • Schmidt erwartet sie schon
    • Lässt sich den Run schildern
    • Lobt den Erfolg
    • Überweist Nuyen



## Charaktere

### Johann “Krone” Mager

K 	G 	R 	S 	W 	L 	I 	C
3	6	5	5	4	3	3	3

#### Kampf:

Initiative 10 + 2d6

Ausweichen 8

Panzerung 9 / Soak 12

10K 

10G

Ares Predator V – Präzision 7 – 8K Schaden – 1 DK – 10 Würfel Angriff

Skalpell – Präzision 6 – 8K Schaden – 3 DK – 8 Würfel Angriff

#### Fertigkeiten: 

Gebräuche 3 – Einschüchtern 6 – Wahrnehmung 3 – Klingenwaffen 2 – Pistolen 4

#### Daten auf Krones Kommlink Stufe 3
    • SIN und Bilder seiner Tochter, wie sie einen verlorenen Milchzahn in die Kamera hält. 
    • Chatverlauf mit Exfrau, die er erschüttert bittet, ihm zu verzeihen, und dass er jetzt in Therapie geht.
    • Ein Bild seiner Tochter, wie sie mit in Fetzen geschnittenem Gesicht auf einem Behandlungsstuhl liegt.




### Vory-Ganger

K 	G 	R 	S 	W 	L 	I 	C
5	4	4	3	2	2	2	2

Initiative: 8 + 1d6 		Ausweichen  6 		Panzerung 9 / Soak 14

10 K/G

Ares Predator V – Präzision 5 – 8K Schaden – 1 DK – 8 Würfel Angriff

