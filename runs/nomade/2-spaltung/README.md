# Der Nomade, Teil 2: Das Dorf

## Background

Die Gegend um Hamm-Uentrop soll platt gemacht werden, weil Saeder-Krupps
Tochter Hochtief ein Industriegebiet bauen will. Im Umland regt sich jedoch
Widerstand, da einige Dörfer ihr Zuhause gerne behalten würden.

Nachdem die Runner den Nomaden aus einem Stützpunkt von Hochtief Security
befreit haben, ist der Widerstand gestärkt. Saeder-Krupp passt das gar nicht,
sie wollen die Bewegung klein machen, um die Kosten des Bauprojekts zu senken.

Deswegen engagiert Frau Schmidt von Saeder-Krupp die Runner, um die Bewegung zu
infiltrieren und zu spalten. Ein Teil der Bewegung wird dabei radikalisiert
werden.

## Charaktere

Nomade - Radu Lumanesc
* Wandermagier
* Reist umher, etwas exzentrisch, lebt von kleinen Zaubereien, mit der Erde
  verbunden
* magisches Refugium hinten im Transporter
* Liebt Freiheit und Unabhängigkeit
* Will das ländliche beschützen, politisch aktiv
* rumänische Herkunft
* Er hat eine erhöhte Konzentrationsfähigkeit und Blitzreflexe - drückt sich
  auch in aufgeweckter Persönlichkeit aus
* Er kommt nicht mit SIMSinn klar und ist da desorientiert
* Voreingenommen gegen Stadtmenschen
* SIN-los, bis S-K ihm eine kriminelle SIN verpasst
* Hat es sich zur Aufgabe gemacht, die Dörfer vor S-K zu schützen
* Erfährt dadurch auch mehr über Politik, Kritik an Konzernen
* Mensch

Edith Schneider - Bürgermeisterin
* Mensch, 50 Jahre alt, trägt eine Anzughose mit langweiliger Bluse. Sie
  ergraut wohl seit einiger Zeit.
* Hat Prinzipien, Rechtsstaat & Familie sind ihr wichtig.
* Ist eine reiche Witwe. Ihr Mann war Exec bei Saeder-Krupp, ist aber vor
  einigen Jahren von Runnern erschossen worden.
* Ist unbestechlich; Frau Schmidt hat bereits versucht, sie zu bestechen.
* Der Ruf ihrer Familie hat für sie oberste Priorität.

Elisa Schneider - Tochter der Bürgermeisterin
* Mensch, 15 Jahre alt. Lange braune Haare, trägt ein rotes Lederjäckchen.
* Sängerin bei The Cooks.
* Hals über Kopf in den Pfarrer verknallt - er hat sie allerdings
  zurückgewiesen.
  * Das hat sie niemandem anvertraut außer der Wirtin und ihrem Tagebuch.

Thomas Ungeduld - Pfarrer
* Elf, 42 Jahre alt. Hochgewachsen, jung und charismatisch. Durch ein großes
  Muttermal auf der Wange wirkt er nicht unnahbar.
* Ist der Pfarrer in St. Elisabeth, Gemeinde in Welver.
* Ihm ist sein eigener Ruf sehr wichtig, und der Support der Gemeinde.
* Die Sicherheit von Dorf und Natur ist ebenfalls eine seiner Prioritäten, noch
  knapp vor seinem Pazifismus.

Ingrid Ingrimm - Wirtin
* Ork, 30 Jahre alt. Kurze schwarze Haare, helle Haut. Sie hat eine
  Stirnbuchse, die offensichtlich beschädigt ist.
* Sie war früher Deckerin, doch ein Virus hat ihr DNI zerbrutzelt. Seitdem will
  sie nie wieder in die Matrix.
* Nachdem sie aus den Schatten raus ist, und sich in Welver zur Ruhe gesetzt
  hat, ist ihr die Sicherheit des Dorfes wichtig.
* Außerdem würde sie nur sehr ungern in den Knast gehen.
* Sie kennt die Geheimnisse der meisten Jugendlichen im Dorf, inklusive
  Eichhörnchens Aufenthaltsort und Elisas geheimer Liebe.

Romina Olafsson
* Eng mit Radu und Eichhörnchen befreundet
* War früher bei einer Spezialeinheit
* Kam dann ins Konzerngefängnis wegen Befehlsverweigerung
* Hat sich nach ihrer Freilassung zur Ruhe gesetzt, aber versteht, dass sie
  sich verteidigen muss

The Cooks - die Dorfband
* Die Cooks bestehen aus:
  * Elisa, der Sängerin
  * Jules, der Gitarristin
  * und Patrick, der Beats macht.
* Früher war noch Eichhörnchen dabei, bis er untertauchen musste. Er spielte
  Bass und schrieb alle Texte.
* Die Texte der Band sind ziemlich Punkig und kritisch, und sie spielen sie oft
  auf Kundgebungen oder in der Wirtschaft.
* Allerdings spielen sie auch im Gottesdienst mit, und covern dafür christliche
  Lieder.

## Ablauf

Einige Wochen später kriegen sie wieder einen Anruf von Mattie, neuer Run. Sie
sollen die Bürgerinitiative Welver spalten. Die Schmidt redet beim Treffen in
Essen viel über die Pommes aus dem Pott, die doch viel besser sind als der
Fisch in Hamburg. Außerdem redet sie viel über Politik, soziale Dynamiken,
Gruppenpsychologie, und dass sie Politik studiert hat. Das Ziel ist, dass die
Bürgerinitiative in der Öffentlichkeit schlecht dasteht oder zerfällt, den
Runnern wird freie Hand gelassen.

Der Nomade hat in Welver mit aufrührerischen Reden den Kampfgeist der Leute
geweckt - deswegen wollen sie um ihr Zuhause kämpfen. Es gibt verschiedene
Sprecher, die aus verschiedenen Gründen und mit verschiedenen Mitteln kämpfen
wollen. Irgendwie muss Zwietracht gesät werden; das Ergebnis ist hier relativ
offen. Der Nomade bleibt auf jeden Fall in dem radikalsten Lager, und als
ehrliche Haut vertritt er auch offen militante Ansätze.

Wenn eine Demonstration gegen das Bauvorhaben scheitert, erachtet Frau Schmidt
die Runner als erfolgreich und ruft sie an. Sie treffen sich wieder in einer
Pommesbude, und erhalten die Bezahlung. Als Frau Schmidt die Rechnung bezahlt,
bedankt sich der Verkäufer bei ihr namentlich mit "Frau Vetter-Marcinak".

## Treffen mit Frau Schmidt

Ein gesittetes Restaurant, aber es gibt nur Pommes. In der Auslage liegen rohe
Kartoffeln. Die AR ist fast leergefegt, nichts soll vom exquisiten Geschmack
der Fritten ablenken.

* Keine Security im Lokal.
* Die Leute, die hier essen, sind ausschließlich Menschen; die Runner fallen
  hier schon sehr auf.
* Frau Schmidt ist förmlich angezogen, ihre Bluse ist hoch zugeknöpft.
* Ihr setzt euch zu ihr, Bug, Shinti, und Shlomo kennen sie bereits. Es ist
  dieselbe Frau Schmidt wie beim Flappflight-Run.
* "Willkommen! Diese Pommes müsst ihr probieren. Sowas gibt es nicht bei euch
  oben in Hamburg, ich sags euch."

Nachdem sie noch ein bisschen über Qualität und Zubereitung von Pommes
schwadroniert hat, fängt sie an, den Auftrag zu beschreiben:

* Hier in der Nähe gibt es ein Dorf namens Welver. Ziemliches Kaff, vllt ein
  paar hundert Einwohner, aber die haben es in sich.
* Die haben eine Bürgerinitiative gegründet, um sich gegen ein Bauprojekt in 
  der Gegend zu wehren. Die halten echt zusammen, das macht es schwierig.
* obwohl ihre Pommes echt nicht so toll sind. Ich hab die in der Kneipe da
  probiert, das ist nicht zu vergleichen mit dem hier.
* Nachdem Versuche, das mit Gewalt und Bestechung zu regeln, bereits
  gescheitert sind, will ich einen kreativeren Ansatz: ich will, dass ihr
  Misstrauen sät, Streit provoziert, und den Zusammenhalt untergrabt.
* Wenn ihr es schafft, das Dorf so zu spalten, dass eine der Demonstrationen
  ein Misserfolg wird, kann ich von da aus weitermachen. Das würde mir reichen.
* Dafür kriegt ihr dann 120.000 EC - 30 Riesen pro Person.
* Ein paar Wochen wird das wohl dauern. Ihr kriegt keinen Cent mehr, nur weil
  ihr länger braucht. Und ich kann euch nicht ewig den Kopf frei halten;
  irgendwann wollen meine Vorgesetzten dann auch Resultate sehen.
* Aber achtet darauf, dass ihr nicht so sehr als Outsider dafür verantwortlich
  gemacht werdet. Das wird den Zusammenhalt im Dorf eher stärken als schwächen.
* Seid ihr dabei?

## Beinarbeit

The Cooks
* Matrixsuche +++
  * 1 Erfolg:  Punkband aus Welver, da spielen 4 Leute mit.
  * 3 Erfolge: Besetzung: Elisa (Gesang), Jules (Gitarre), Eichhörnchen (Gesang
    & Bass), und Patrick (Beats).
  * 4 Erfolge: Bei den letzten paar Auftritten war Eichhörnchen anscheinend
    nicht mehr dabei.
* Wenn man bei Eichhörnchen nachfragt: 
  * 0 Erfolge: Wir wollten immer was politisches machen. Die anderen haben das
    nicht so krass gesehen, aber mir war das immer sehr wichtig.
  * 2 Erfolge: Wir haben oft in der Kneipe gespielt, und weil Elisa wollte,
    auch im Gottesdienst. Lol. Aber da haben wir dann mehr gecovert, und
    weniger von meinen Liedern gespielt.
* Aus der Aktivistendatenbank: +++
  * Fordern in ihren Liedern teilweise zum mehr oder weniger friedlichen
    Aufstand auf.
  * Haben gute Connections zur Kirche und zu Thomas Ungeduld.
  * Der Bassist wird wegen Brandstiftung gesucht und ist untergetaucht.

Edith Schneider (CSVP)
* Matrixsuche +++
  * 0 Erfolge: Die Bürgermeisterin von Welver heißt Edith Schneider und gehört
    der CSVP an.
  * 1 Erfolg:  "Vernunft, Rechtsstaat, Familie - wir sorgen dafür, dass ihre
    Lieben sicher bleiben!"
  * 2 Erfolge: Ehemann hat bei Saeder-Krupp gearbeitet und ist vor ein paar
    Jahren von Runnern ermordet worden.
* Wenn man jemand aus dem Dorf fragt:
  * 0 Erfolge: Das ist halt unsere Bürgermeisterin. Sie ist ziemlich beliebt,
    letzendlich macht sie schon vernünftiges Zeug.
  * 2 Erfolge: Ihre Tochter spielt bei den Cooks.
* Aus der Aktivistendatenbank: ++
  * Finanziert die Kundgebungen. Ihr Dorf liegt ihr sehr am Herzen.
  * Sie ist eine reiche Witwe und anscheinend absolut unbestechlich.

Thomas Ungeduld - Pfarrer
* Wenn man jemand aus dem Dorf fragt:
  * 0 Erfolge: Ist echt ein netter Pfarrer. Ich glaube zwar nicht an Gott und
    gehe nicht in die Kirche, aber netter Typ.
  * 3 Erfolge: Nee, der hat wirklich gar keinen Dreck am Stecken. Entweder
    weil, oder obwohl er ein Mann Gottes ist.
* Aus der Aktivistendatenbank: +
  * Einer der Wortführer. Hetzt seine Gemeinde auf und redet auch oft auf
    Kundgebungen.

Frau Vetter-Marcinak
* Matrixsuche:
  * 0 Erfolge: Es gibt im Ruhrgebiet 135 Frauen mit diesem Nachnamen
  * 1 Erfolge: Arbeitet für Saeder-Krupp als "Juristische Beratung".
  * 3 Erfolge: Hat Politikwissenschaften studiert und dann direkt bei S-K
    angefangen
  * 5 Erfolge: Wohnt alleine in einem 3-Zimmer-Apartment in Neu-Essen. Adresse
* Schatten-Kontakte:
  * 2 Erfolge: Ja, bei der hatte ich auch schon einen Run. Ziemlicher
    Frischling, die ist harmlos. Ein paar verrückte Ideen hat sie.
  * 4 Erfolge: Passt echt auf, dass sie nichts über euch erfährt. die ist
    löchrig wie ein Sieb, jeder könnte aus der Infos rauskriegen. Sehr
    unvorsichtig.

## Das Dorf

Um nach Welver zu kommen, muss man durch Hamm-Uentrop durch. Ihr lasst die
Kühltürme hinter euch, als ihr tiefer in die Provinz kommt. Das Kaff sieht
ziemlich verschlafen aus, hat wohl ein paar hundert Einwohner. Ihr seht einen
Kirchturm, und an der Hauptstraße ist eine Kneipe. Sie hat offen.

Neben der Kneipe, die eigentlich immer offen hat, gibt es einige regelmäßige
Veranstaltungen in Welver: Der Gottesdienst, die wöchentliche Kundgebung, und
der Partyabend. Sie folgen einer gewissen Reihenfolge, die auch mal abweichen
kann:

### Kundgebung 

Mittwoch 14:00. Geht im Dorf los. Sowohl Dorfjugend als auch Gemeinde sind da.

* Musik wird abgespielt
* Redebeiträge
* zur Baustelle gehen (ca. 15 Minuten)
* Bäume pflanzen
* Redebeiträge
* Die Band spielt
* Versammlung löst sich auf

### Kneipenabend

Fängt immer Freitag gegen 20:00 an. Vorher trinken auch noch ältere Leute da,
aber ab 20:00 sind eig nur noch junge da.

* ältere Leute trinken
* jüngere kommen dazu
* ältere gehen langsam
* Die Band spielt
* Happy Hour
* alle sind besoffen
* i-wann gehen alle nach Hause

### Gottesdienst

Sonntag 10:00 in der Kirche. Da ist nur die Gemeinde und die Cooks anwesend.

* The Cooks spielen
* Beten
* The Cooks spielen
* Abendmahl
* The Cooks spielen
* Predigt
* The Cooks spielen
* gemeinsames Kaffee trinken

## Wie soll man das Dorf spalten?

Nun, die Runner gewinnen, wenn eine Kundgebung wegen interner Streitigkeiten
schief läuft. Als Spielleiter kann man das natürlich sehr weit interpretieren,
und die Spieler wissen das. Wenn sie eine coole Idee haben, dann lässt man
natürlich durchscheinen, ob man sie für gut genug hält.

Das kann alles mögliche sein. 
* Vielleicht stecken sie der Bürgermeisterin, dass ihre Tochter in den Pfarrer
  verliebt ist - und die verbietet ihr, in der Kirche aufzutreten.
* Vielleicht kommt es in der Kirche und auf den Kundgebungen zunehmend zu
  Diskussionen über Gewalt oder den Nahostkonflikt, und die Bürgermeisterin
  kann die Bewegung irgendwann nicht mehr tragen.
* Es passieren unredliche magische Dinge, und das Dorf schiebt es dem Nomaden
  in die Schuhe, der darauf hin beleidigt ist und irgendwas sabotiert oder
  destruktiv stichelt.
* .... den Runner/innen wird hier mehr einfallen.

In der Bewegung ist die Bürgermeisterin für den finanziellen Support
verantwortlich, der Pfarrer für die Leute und das Gefühl der gemeinsamen
Ermächtigung, der Nomade für den Pepp und die Ideen, und the Cooks für die
Mobilisierung der Jugend.

Wenn ein oder zwei davon wegbrechen oder gestört werden, könnte die Bewegung
bereits empfindlich geschwächt sein. Als Spielleiter/in sollte man das so
interpretieren, dass möglichst viel Spielspaß ankommt.

Wenn die Runner/innen es geschafft haben, erhalten sie einen Anruf von Frau
Schmidt.

## Win!

Sie treffen sich wieder in einer Pommesbude, und erhalten die Bezahlung. 

Als Frau Schmidt die Rechnung bezahlt, bedankt sich der Verkäufer bei ihr
namentlich mit "Frau Vetter-Marcinak".

