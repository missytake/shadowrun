
# Charaktere Nomade - Radu Lumanesc
* Wandermagier
* Reist umher, etwas exzentrisch, lebt von kleinen Zaubereien, mit der Erde
  verbunden
* magisches Refugium hinten im Transporter
* Liebt Freiheit und Unabhängigkeit
* Will das ländliche beschützen, politisch aktiv
* rumänische Herkunft
* Er hat eine erhöhte Konzentrationsfähigkeit und Blitzreflexe - drückt sich
  auch in aufgeweckter Persönlichkeit aus
* Er kommt nicht mit SIMSinn klar und ist da desorientiert
* Voreingenommen gegen Stadtmenschen
* SIN-los, bis S-K ihm eine kriminelle SIN verpasst
* Hat es sich zur Aufgabe gemacht, die Dörfer vor S-K zu schützen
* Erfährt dadurch auch mehr über Politik, Kritik an Konzernen
* Durch Repression wird er radikalisiert
* Wenn der Nomade radikalisiert ist, neigt er auch zu Rache
* Als toxischer Schamane verdreht sich seine Liebe zur Natur ins
  antizivilisatorische; 
  * gegen Gesellschaft, Fortschritt, und Technologie
* Mensch

Frau Schmidt
* Arbeitet für Saeder-Krupp und Hochtief
* Hat Politikwissenschaft studiert
* Kennt die Runner schon vom Flappflight-Run
* Ist noch nicht lange dabei und manchmal etwas nachlässig, was Sicherheit
  angeht
* Ist fasziniert von Pommes, und probiert gerne unterschiedliche Buden aus
* Mensch

Herr Schmidt
* Arbeitet für Alamais, wurde von ihm magisch manipuliert
* Ist bei NeoNet als Spinne angestellt
* Steht auf die Matrix und verbringt gerne seine Zeit in der VR
* Hat durch VR-Sucht (und Alamais' Manipulation) ein paar Löcher im Verstand
* seine Persona sieht aus wie Duke Nukem
* Etwas fanatisch unter einer aufgeklärten Oberfläche
* Tut so, als würde er rational auf ein Ziel hinarbeiten, aber eigentlich folgt
  er nur Befehlen, die ihm völlig unverständlich sind, und setzt sie dann in
  seinem Kopf zu einer konsistenten Geschichte zusammen.
* Elf

Mattie der Fixer
* Mensch
* Hat einen Stuffer Shack in Hamburg
* Schmierig, wird aber schnell unfreundlich, wenn man nicht spurt

# Background

Hochtief, eine Tochterfirma von S-K, will Dorf plattmachen für Bauland.
Alamais will S-Ks Geschäft kaputtmachen. Der Nomade will das Dorf Welver bei
Hamm schützen.

Wenn Alamais den Runnern Aufträge gibt, legt er Spuren, dass der Auftraggeber NeoNet ist.

Runs:
* Alamais: Baupläne bei S-K klauen, Nomade befreien
  * Nomade gründet Bürgerinitiative, weil SK ein Dorf plattmachen will
* Saeder-Krupp/Hochtief: Bürgerinitiative ausspionieren & Spalten
  * Der Nomade landet bei der radikalen Hälfte der Bürgerinitiative
* Alamais: mit Nomade bei S-K einbrechen, Atomunfall beim AKW Hamm-Uentrop von
  S-K herbeiführen
  * Nomade kriegt die Ursache mit, wird toxisch
  * Nomade versucht jetzt herauszufinden, wer Runner beauftragt hat; jagt erst
. Fixer, dann Alamais-Schmidt
* SK: Nomaden jagen und töten, bevor er SK-Schmidt auf die Pelle rückt
  * Nomade verrät den Runnern im letzten Atemzug, dass sie für Alamais arbeiten
* falls Runner sich darüber aufregen, fängt Alamais an, sie zu jagen

## Prolog

Der Nomade hat versucht, astral zu spionieren, um herauszufinden, was Hochtief
plant, um das Dorf Welver plattzumachen. S-K Lohnmagier haben ihn anhand
seiner astralen Signatur aufgespürt, und ihn von einem kleinen Runnerteam
festnehmen lassen. Er sitzt nun erstmal in Uentrop, in der Nähe des AKWs, bei
einem Security-Standort von Hochtief in Gewahrsam.

## 1. Run: Die Baupläne

Die Runner kriegen einen Anruf von Mattie, dem Fixer. Er sagt, dass sie den
Schmidt in Essen treffen werden, und dass sie sich sofort auf den Weg machen
müssen. Sie treffen Herr Schmidt in einer Pommesbude in der Öffentlichkeit,
keine Bodyguards. Auftrag: "Holt die Baupläne für das AKW aus dem
Hochtief-Security-Standort, bevor sie morgen routinemäßig gelöscht werden".
(Sie werden eigentlich nie gelöscht, das ist eine Unstimmigkeit).

In dem Security-Standort treffen die Runner auf einen Lohnmagier, der sie
aufspürt, und den Alarm lostritt. Nachdem sie ihn getötet haben, ist die
magische Sperre auf dem Nomaden weg. Sie finden den Nomaden in einer Zelle,
deren Tür über die Matrix gesichert ist.Entweder der Nomade schafft es so, sie
zu überzeugen, ihn freizulassen, oder er nutzt den Beeinflussen-Zauber.

Sie finden die Baupläne auf dem Host in einem Archiv, zusammen mit einigen
Hintergrundinfos, und hauen ab. Sie übergeben die Baupläne Herrn Schmidt in
der VR, in einem NeoNet-Host.

## 2. Run: Die Bürgerinitiative

Einige Wochen später kriegen sie wieder einen Anruf von Mattie, neuer Run. Sie
sollen die Bürgerinitiative Welver spalten. Die Schmidt redet beim Treffen in
Essen viel über die Pommes aus dem Pott, die doch viel besser sind als der
Fisch in Hamburg. Außerdem redet sie viel über Politik, soziale Dynamiken,
Gruppenpsychologie, und dass sie Politik studiert hat. Das Ziel ist, dass die
Bürgerinitiative in der Öffentlichkeit schlecht dasteht oder zerfällt, den
Runnern wird freie Hand gelassen.

Der Nomade hat in Welver mit aufrührerischen Reden den Kampfgeist der Leute
geweckt - deswegen wollen sie um ihr Zuhause kämpfen. Es gibt verschiedene
Sprecher, die aus verschiedenen Gründen und mit verschiedenen Mitteln kämpfen
wollen. Irgendwie muss Zwietracht gesät werden; das Ergebnis ist hier relativ
offen. Der Nomade bleibt auf jeden Fall in dem radikalsten Lager, und als
ehrliche Haut vertritt er auch offen militante Ansätze.

Wenn eine Demonstration gegen das Bauvorhaben scheitert, erachtet Schmidt die
Runner als erfolgreich und ruft sie an. Sie treffen sich wieder in einer
Pommesbude, und erhalten die Bezahlung. Als Frau Schmidt die Rechnung bezahlt,
bedankt sich der Verkäufer bei ihr namentlich mit "Frau Vetter-Marcinak".

## 3. Run: Das Kraftwerk

Über Mattie meldet sich Herr Schmidt wieder bei den Runnern. Sie treffen sich
in der Matrix. Die Runner sollen in das AKW Hamm-Uentrop einbrechen, das S-K
gehört, und einen Virus hochladen. Herr Schmidt erzählt ihnen, dass der Virus
den Reaktor abschaltet und unbrauchbar macht, und eine politische Botschaft an
die Kühltürme strahlt. Weil es magische Schutzvorrichtungen (auch gegen die
Strahlung) gibt, brauchen sie den Nomaden, um sie mit einem Ritual zu
deaktivieren.

Die Runner müssen also den Nomaden davon überzeugen, mitzumachen. Wenn das
gelingt, fahren sie mit dem Transporter in die Nähe, und die Magier führen
hinten drin das Ritual durch. Das Ritual ist ein spezieller Zauber, der die
Geister der Natur anruft, um die Magie der Zivilisation ins Wanken zu bringen.
Nur der Nomade kennt ihn, und er schlägt vor, ihn zu benutzen. Das Ritual
setzt hermetische Magie jeder Art in dem Bereich für mindestens 6 Stunden außer
Kraft.

Wenn die magischen Schutzvorkehrungen erledigt sind, können die Runner in das
AKW eindringen. Alle Matrixgesteuerten Geräte sind aus Sicherheitsgründen
direkt miteinander verkabelt. Wenn die Verbindung allerdings zusammenbricht,
verbinden sie sich auch über Wifi miteinander. Der Virus ist ein
offensichtlich von NeoNet entwickelter Agent, der nacheinander die richtigen
Schalter umlegt, um das AKW hochzujagen. Der Prozess dauert 4 Stunden und löst
nach 15 Minuten einen Alarm aus, den man innerhalb der Anlage hören kann.

Der Nomade bricht in Panik aus und versucht um jeden Preis zu entkommen. Dabei
kümmert er sich nicht um die Runner, und kooperiert vielleicht nicht mal mit
ihnen.

Der Atomunfall ist nicht unglaublich katastrophal, bald greifen die magischen
und technischen Schutzmaßnahmen ein. Doch im Umfeld von 15 Kilometern wird die
Landschaft von einer nuklearen Druckwelle erfasst, die praktisch alles
zerstört. Damit verschwinden nicht nur das AKW und das Dorf Welver vom
Erdboden, auch die Städte Hamm, Ahlen, und Beckum sind vom Erdboden
verschluckt. Wenn die Runner dann noch im Umkreis sind, erleiden sie 12K
nuklearen Schaden.

Nomade
* Wird toxisch
* will sich an den Hintermännern des Runs rächen
* verhört einen der Runner magisch
* jagt daraufhin Mattie, Herr und Frau Schmidt (in der Reihenfolge)
* verhört Herrn Schmidt und findet raus, dass Alamais dahintersteckt

Alamais
* Herr Schmidt hat seinen Zweck erfüllt. Er muss sterben, bevor er
  herumerzählt, was er weiß.
* Hetzt ein paar Nazis auf Herrn Schmidt
* Hasskampagne gegen Lofwyr und S-K geht los

Herr Schmidt
* Stimme in seinem Kopf ist plötzlich weg und hinterlässt nur Löcher
* Sieht, was er getan hat, und flieht panisch, weiß aber nicht wirklich wohin
* Den Credstick, den er den Runner wirklich geben wollte, nimmt er für die
  Flucht mit

Mattie
* Hat keine Provision bekommen, sauer auf Herr Schmidt

Frau Schmidt
* BI ist weg: gut
* PR-Desaster: schlecht
* Sie vertraut den Runnern, alle Beweise wurden ja in der Atomexplosion
  vernichtet
* Der Nomade hat ihren Namen von den Runnern und jagt sie
* Sie beauftragt die Runner, den Nomaden zu jagen

Wenige Stunden nach dem Atomunfall werden die Runner von Mattie angerufen.
Herr Schmidt ist untergetaucht, ohne Matties Provision zu zahlen. Mattie ist
stinksauer. Er gibt ihnen Namen und Adresse von Herrn Schmidt, sie sollen ihn
finden.

Herr Schmidts schickes Haus ist verlassen, er ist offensichtlich ruckartig
aufgebrochen. Die Runner finden verschiedene Hinweise darauf, dass er
anscheinend echt durch ist. NeoNet ist offensichtlich nur sein Dayjob, nicht
der Auftraggeber der Runs. Die Wände sind vollgeschrieben mit irgendwelchen
Runen. An einer Wand hängt eine Mindmap mit verschiedenen Daten über S-K -
Lofwyr ist in der Mitte. In der Garage ist ein Auto registriert, wenn die
Runner es tracken, können sie den Standort ermitteln.

Herr Schmidt ist auf dem Weg nach Norden, aber wechselt offensichtlich
regelmäßig die Richtung. Er weiß auch nicht so genau, wo er eigentlich hin
will, und ändert seine Meinung regelmäßig.

Kurz bevor die Runner ihn erreichen, hört er auf, sich zu bewegen. Sie finden
sein Auto auf einem Parkplatz abgestellt. Ein paar (bewaffnete) Nazis sind
gerade damit beschäftigt, sein Auto zu durchsuchen. Seine Leiche ist stark
verbrannt, die astrale Signatur des Nomaden hängt in der Luft. Den Credstick
haben die Nazis. In ihrem Kommlink finden die Runner den Auftrag von ihrem
Boss, Herrn Schmidt umzubringen, "weil er ein Scheißelf ist und sie keine
Fragen stellen sollen".

## 4. Run: Der Zusammenbruch

Zwei Tage später kriegen die Runner einen panischen Anruf von Frau Schmidt,
dass jemand hinter ihr her ist. Der Nomade hat offensichtlich ihre Wohnung
verwüstet, ist aber auf den Überwachungskameras drauf. Sie gibt den Runnern
den Auftrag, ihn zu töten, bevor er sie findet.



Der Nomade verrät den Runnern im letzten Atemzug, dass sie für Alamais
arbeiten.

# Thoughts

About deals with dragons: https://www.reddit.com/r/Shadowrun/comments/77jqbt/never_ever_cut_a_deal_with_a_dragon/

Lofwyr should be pretty pissed about them working for Alamais, and about
the whole nuclear explosion thing. Would he want to pay Alamais back for
this? Would he use the runners?

Alamais on the contrary should be pretty generous that they killed the
nomad. - no, he uses people as tools, not more

Maybe all of this fits in a follow up campaign, where Alamais puts the
runners together with some nazis, so they want to get out of Alamais'
service - maybe that's what Lofwyr uses as incentive so the runners
switch sides.

Okay, let's put this in another campaign, lol. this one is getting out
of hand anyway :D

Warum sollten die Runner den Nomaden auf den Run gegen die Atomzentrale
mitnehmen? Er soll ja vom Effekt des Runs eh nichts wissen. Die Runner am
besten auch nicht. Ist Sabotage das eigentliche Ziel des Runs?

