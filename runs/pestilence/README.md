# Pestilence

Pestizide sind nützlich für alle - die sie sich leisten können. Aber wenn die
Formeln kostenlos wären? Dann wären unabhängige Bäuer\*innen nicht mehr
abhängig von Großkonzernen wie der AG Chemie.

[Charaktere](chars.md)

## Hintergrund

Avanya hat Kanta eine Kopie der Kommlink-Daten der Pestizid-Test-Abteilung und
der Innenrevision der AG Chemie zukommen lassen. In denen stehen
vielversprechende erste Daten zu "Pestilence", einem neuen Produkt der
Pestizid-Abteilung.

Außerdem stehen in den Daten die Ergebnisse eines Sicherheits-Pen-Tests, die
noch nicht veröffentlicht sind, in dem einige Schwachstellen der
Forschungsabteilung stichpunktartig aufgeführt sind:
- Die Hintertür hat den Code 1111 (why? WHY?)
- Nachts sind nur zwei Metamenschen da, um im Zweifel intervenieren zu können
- Der Rigger ist vor Ort - wenn er ausgeschaltet wird, verlieren die Drohnen
  ihre Effektivität

Kanta beauftragt deswegen die Runner, diese Daten zu stehlen, damit
Agrar-Kollektive endlich gute Pestizide bekommen können, und die anarchistische
Lebensmittelproduktion effektiver wird. Außerdem sollen die Formeln
veröffentlicht werden, damit alle unabhängigen Bäuer\*innen damit arbeiten
können.

Was nicht in diesen Daten steht: kurz nach Ablauf des Probezeitraums wird das
Pestizid aggressiv und schädlich, und man muss das mildernde Zusatzprodukt
"famine" erwerben, um die eigene Ernte und die Zukunft des Bodens zu retten.
Dieses "Feature" wurde von Natalie Medved in Auftrag gegeben, um Bäuer\*innen
an die AG Chemie zu binden. Theodor Küste soll es im Alleingang entwickeln,
weil solche illegalen Machenschaften nicht das ganze Team wissen muss.

Alwina Sommer fallen diese Unregelmäßigkeiten in den Tests auf. Sie weiß nichts
von dem mildernden Zusatzprodukt, weswegen ihr die Erkenntnisse große Sorgen
machen. Natalie übergeht diese Bedenken, weswegen Alwina sich die Nächte um
die Ohren schlägt, um das Ausmaß des Problems zu beweisen.

Die Formeln sind gut verschlüsselt. Die Passwörter werden jeden Abend geändert,
und erst früh morgens an die Forscher\*innen herausgegeben. Solange sind sie
nur auf Natalie Medveds Kommlink gespeichert.

Auf ihrem Kommlink finden sich außerdem Informationen zu "famine", sie hat mit
Theodor Küste ausführlich darüber geschrieben. Sie hat mehrfach mit Kiefer über
die geplante Entführung von Sandro Mauer und Anton Duskin telefoniert (siehe
[blood money](../bloodmoney/)).

Welche Leute gibt es in der Abteilung, welches Interesse haben sie?
- Boss (Janus Schmied-Huber) (39) will aufsteigen, braucht ein erfolgreiches Produkt
- Forscherin Alwina Sommer (28) kam über "Women in Tech"-Programm rein, hat Bedenken, wird nicht zugehört
- Projektmanagerin Natalie Medved (24) will ihren Karriereknick gradebiegen und das auf jeden Fall durchboxen
  - Sie hat zuletzt zwei Kollegen aus den Prüfungsabteilungen kidnappen lassen, aber wurde nicht entlarvt
- Forscher Theodor Küste (63) hat sich immer nur über Plagiate hochgearbeitet, und ist einfach inkompetent
- Rigger Utz Atzenstamm (22), Ork. Süchtig nach VR-Spielen, mit denen er die Nachtschichten verbringt, neigt zu übertriebener Gewalt ohne Rücksicht auf Kollaterale.
- Mark Theissen (18), Adept im Sicherheitsteam, ist ein alter Klassenkamerad von Tapir

- Nachts vor allem Drohnen, und nur 2 Secus, die vor den Kameras sitzen
- In Natalie Medveds Kontakten ist der Link zu den 'Living Dead'
- Passwort zur Formel ändert sich jeden Tag, das neue ist nur in Natalie Medveds Kommlink gespeichert und wird erst morgens rumgeschickt

## Kanta hat einen Job

Kanta ruft Avanya an: "Hey, die Daten, die du mir neulich geschickt hast, waren
ziemlich wertvoll. Interesse an einem Job?"

"Frag doch noch die beiden, mit denen du überhaupt an die Daten gekommen bist -
wenn sie schon drin sind, macht es das vielleicht einfacher. Wir treffen uns
morgen um 20:00 Uhr im Schockwellenreiter-Forum, im Makhno-Raum."

Schockwellenreiter-Forum
- eine Art Labyrinth ohne Wände
- Bodenplatten schweben im luftleeren Raum und bewegen sich, um Wege zu bilden
  oder wieder zu schließen
- Ein Türrahmen bilden sich aus dem nichts, Neonlicht schimmert hindurch
- Du musst an den Raum denken, in den du willst, bevor du durch eine der Türen
  gehst.

Makhno-Raum
- alte Eisenbahn, donnert durch die ukrainische Prärie
- durch die Tür landet man in einem klapprigen Waggon 
- in einem Abteil räkelt sich eine alte Dame mit scharfem Blick, vor sich Tee

Wenn alle da sind:
- anarchistische Agrar-Kollektive brauchen endlich gute Pestizide 
- damit Lebensmittelproduktion effektiver wird, unabhängig von Konzernen werden
- Außerdem sollen die Formeln veröffentlicht werden, damit alle unabhängigen
  Bäuer\*innen damit arbeiten können.
- "pestilence" ist der Arbeitstitel eines neuen Pestizids, das ziemlich
  vielversprechend klingt
- wenn wir die Formel veröffentlichen, bevor es patentiert wird, können die
  Bäuer\*innen nicht in Abhängigkeit gezwungen werden
- Wir haben einige Tipps für Schwachstellen in der Sicherheit vom Frankfurter
  Forschungszentrum der AGC Crop Science.
- Damit sollte es relativ leicht sein, an die Formel von "pestilence" zu kommen
- Groß ist unser Budget leider nicht, aber es ist für einen guten Zweck: 5000
  Nuyen pro Person.
- Seid ihr interessiert?

Wir haben Stichpunkte für einen Sicherheits-Pentest, der noch nicht
veröffentlicht ist - die Lücken sind wahrscheinlich noch offen:
- Die Hintertür hat den Code 1111, wurde wohl vergessen.
- Nachts sind nur zwei Metamenschen da, um im Zweifel intervenieren zu können,
  größtenteils Drohnen
- Der Rigger ist vor Ort - wenn er ausgeschaltet wird, verlieren die Drohnen
  ihre Effektivität
- Im Host sollte es auch einen Gebäudeplan geben, da sind wir leider nicht
  drangekommen.
- Je schneller ihr den Run durchführt, desto unwahrscheinlicher ist es, dass
  sie die Sicherheitslücken geschlossen haben.

## Beinarbeit

AGC Crop Science, Forschungszentrum Frankfurt (Was redet man in den Schatten?)
- 0: Das Forschungszentrum ist am Rand des AG Chemie-Konzerngeländes in
  Ludwigshafen, Groß-Frankfurt. Man muss nur über einen Zaun und steht im
  Garten.
- 1: Der Matrix-Host hat eine relativ hohe Stufe, aber keine Zähne. Die
  verlassen sich auf ihre Spinne.
- 2: Nachts ist die Spinne gar nicht vor Ort, nur der Rigger. Mit dem ist
  allerdings nicht zu spaßen.
- 3: Der Sicherheitschef ist so ein Jungspund von der Adeptenschule - hat aber
  wenig Praxis.
- 4: Grundriss der Forschungseinrichtung

Natalie Medved
- 0: Mensch, 24 Jahre alt, arbeitet bei der AGC Crop Science.
- 2: Natalie arbeitet als Projektmanagerin in der Abteilung Pestizide.
- 4: Karriereknick vor einem halben Jahr. War vorher für Medikamente zuständig,
  da gab es aber Probleme mit Nebenwirkungen.

Mark Theissen
- 1: Mensch, 18, Sicherheitsverantwortlich bei AGC Crop Science in Frankfurt.
- 3: Hat die AG Chemie-Adeptenschule absolviert, mit sehr guten Noten.

Utz Atzenstamm
- 1: Ork, 22 Jahre alt, Sicherheitsrigger.
- 2: Seine Fakebook-Chronik ist voll mit Highscores von Ballerspielen. Vor allem Nachts.

Alwina Sommer (Fakebook)
- 1: Mensch, 28 Jahre alt, arbeitet als Forscherin bei AGC Crop Science
- 3: Absolventin eines "Women in Tech"-Programms
- 4: Schwester leidet an Folgeschäden von Pestizidvergiftung

Dr. Theodor Küste
- 2: Mensch, 63 Jahre alt, arbeitet als Forscher bei AGC Crop Science
- 4: Seine Doktorarbeit ist auf einer unabhängigen Rezensionsplattform als
  Plagiat abgestempelt.

Janus Schmied-Huber 
- Zwerg, 39 Jahre alt, Leiter des Frankfurter Forschungszentrums
- Kontakt AG Chemie: will aufsteigen, braucht ein erfolgreiches Produkt

## Forschungscampus

Das Forschungszentrum ist am Rand des AG Chemie-Konzerngeländes in
Ludwigshafen, Groß-Frankfurt. Man muss über einen Zaun, und steht hinten im
Garten.

Das Gebäude ist hell, abgerundet und flach. Hinten ist ein Tiergehege, und eine
kleine Hintertür. Im Tiergehege grasen ein paar Schafe.

In den Arbeitsbereichen (6) sieht Alwina Sommer abermals ihre privaten
Testergebnisse durch. In Raum 16 schläft Mark Theissen. in Raum 14 sitzt Utz
Atzenstamm und zockt.

Im vorletzten Manager-Office (20), dem von Natalie Medved, liegt ihr
ausgeschaltetes Kommlink.

## Matrix-Sicherheit

Der Host sieht von innen aus wie eine Pflanzenzelle.
- In der Mitte schwebt der Zellkern
- Mitochondrien, Chloroplasten, und einzelne Proteine treiben im Raum herum
- Um den Zellkern ist endoplasmatisches Retikulum, das anscheinend den Lageplan des Gebäudes darstellt
- Die Ribosomen scheinen die Türen zu sein
- Ein Virus schwebt umher und dockt immer mal wieder an einzelne Zellteile an

Wie funktioniert der Host?
- Daten sind im Zellkern gespeichert
- Zellwand verhärtet sich bei Linksperre
- Dictyosom bildet IC - IC sieht aus wie kleine Viren

Im Zellkern liegen die Arbeitsdaten von Projekt "pestilence":
- Schichtpläne
- README: das Passwort für die Forschungsdaten kriegt ihr jeden Morgen von
  Natalie. Kommlinks müssen ausgeschaltet in den Büros gelassen werden.
- Versuchsergebnisse sind verschlüsselt mit Schutzstufe 3
- Pestilence ist verschlüsselt mit Schutzstufe 8
- Famine ist verschlüsselt mit Schutzstufe 4

Host Stufe 7: A8 S7 D10 F9
- Matrix-Strategie: Patrouille, Teerbaby, Aufspüren, Drohnen drauf hetzen, Leuchtspur, Killer
- Die Sicherheitsspinne kommt 3+1d6 Kampfrunden nach dem Alarm
- Kameras Stufe 2, Türen Stufe 2, Schleusen Stufe 3

Sicherheitsrigger ist vor Ort, im Matrix-Sicherheits-Office

Verteidigungsdrohnen (alle Stufe 3)
- 3 GM-Nissan Dobermann mit HK-227 Maschinenpistolen
- 2 Rotor-Drohnen mit Ares Alpha + Granatwerfer sind auf dem Dach stationiert
- 1 Ares Duelist mit zwei Schwertern, die 7K-2 Schaden machen.

