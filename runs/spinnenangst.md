# Spinnenangst

Der Zwerg Magnus Kuhn wurde bei der AG Chemie gefeuert - allerdings nicht ohne
sich einige Firmengeheimnisse unter den Nagel zu reißen. Um ein neues Leben
anzufangen, will er nun seinen alten Arbeitgeber damit erpressen und sich mit
dem Geld absetzen.

Die Runner:innen sollen die Übergabe durchführen - allerdings spielen beide
Seiten mit gezinkten Karten, ohne dass die Runner:innen das wissen. Denn die AG
Chemie denkt gar nicht daran, zu bezahlen, der Credstick entpuppt sich als
Rauchbombe und eine unsichtbare Person entreißt den Runner:innen den
Datenstick.

Doch auch Magnus Kuhn hatte nicht vor, sich an den Deal zu halten. Der
Datenstick ist in Wirklichkeit ein kleiner Spinnenbot, der sich kurz nach der
Übergabe selbstständig macht um an einen vorprogrammierten Ort zu fliehen. Kuhn
bittet die Runner:innen, den Bot dort abzuholen.

Doch die AG Chemie ist ziemlich beleidigt, gelinkt worden zu sein, und ist den
Runner:innen dicht auf den Fersen. Sie werden beschattet - so wird es nicht
trivial, die Daten wieder einzusammeln, und es kommt noch einmal zum Showdown
mit dem AG Chemie-Team.

Ob die Runner:innen sich darauf einlassen die Daten zurückzuholen, und was sie
dann damit machen hängt von ihnen ab - Kuhn kann sie nicht bezahlen, und die AG
Chemie scheint in der Sache auch nicht vertrauenswürdig zu sein.
Veröffentlichen? Selbst nutzen? An die Konkurrenz verkaufen? Vielleicht haben
sie auch eigene Kontakte die etwas damit anfangen können?

